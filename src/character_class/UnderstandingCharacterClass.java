package character_class;

import utilities.ScannerHelper;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {
        /*
        Char is primitive data type
        Character is a wrapper class
        Character is object representation of char primitive

        Wrapper classes provides us some methods that allows to manipulate the data
         */
        String str = ScannerHelper.getString();
        //Print true if str starts with uppercase, print false otherwise

        // 1st Way
        System.out.println(str.charAt(0) >= 65 && str.charAt(0) <= 90);

        // 2nd Way
        System.out.println(Character.isUpperCase(str.charAt(0)));//turns boolean
        System.out.println(Character.isLowerCase((str.charAt(0)))); // turns boolean
        System.out.println(Character.isLetter(str.charAt(0)));
        System.out.println(Character.isLetterOrDigit(str.charAt(0)));
        System.out.println(Character.isDigit(str.charAt(0)));
        System.out.println(Character.isWhitespace(str.charAt(0)));
    }
}
