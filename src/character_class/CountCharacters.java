package character_class;

import utilities.ScannerHelper;

public class CountCharacters {
    public static void main(String[] args) {
        /*
        write a method that ask user to enter a String
        Count letters
        Count digits
        Ex;
        "123 Street Chicago" -> This string has 3 digits and 13 letters
        "ABC"                -> This string 0 digits and 3 letters
        "12345"              -> This string has 5 digits 0 letter
        "      " 0 digit and 0 letter
         */
        String str = ScannerHelper.getString();
        int digit = 0;
        int letter = 0;
        for (int i = 0; i < str.length(); i++) {
            if(Character.isDigit(str.charAt(i))) digit++;
            else if(Character.isLetter(str.charAt(i))) letter++;
        }
        System.out.println("This string has " + digit + " digits and " + letter + " letters");

        System.out.println("\n-------task2-----\n");

        /*
        Write a method that asks user to enter a String
        Uppercase letters
        Lowercase letters

        Examples
        "123 Street Chicago"    -> This string has 2 uppercase letters and 11 lowercase letters
        "ABC"                   -> This string has 3 uppercase letters and 0 lowercase letters
        "12345"                 -> This string has 0 uppercase letters and 0 lowercase letters
        "     "                 -> This string has 0 uppercase letters and 0 lowercase letters

        */
        int upper = 0, lower = 0;
        for (int i = 0; i <= str.length() - 1 ; i++) {
            if(Character.isUpperCase(str.charAt(i))) upper++;
            else if (Character.isLowerCase(str.charAt(i))) lower++;
        }
        System.out.println("The string has " + upper + " uppercase letters and " + lower + " lowercase letters");

        System.out.println("\n-------task3-----\n");
         /*
         Write a program that asks user to enter a String
         Count how many special characters you have in the String

         "Hello World"      -> 0
         "Hello!"           -> 1
         "abc@gmail.com"    -> 2
         "!@#$%&"           -> 6

          */

        int specialC = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if(!Character.isLetterOrDigit(c) && !Character.isWhitespace(c)) specialC++;

        }
        System.out.println(specialC);

        System.out.println("\n-------task4-----\n");

        /*
        Putting all together

        Write a program that asks user to enter a String

        Count letters
        Count uppercase letters
        Count lowercase letters
        Count digits
        Count spaces
        Count specials
        The price of the iPhone is $1,000.
        Letters = 21
        Uppercase letters = 2
        Lowercase letters = 19
        Digits = 4
        Spaces = 6
        Specials = 3
        */
        int let = 0, up = 0, low = 0, dig = 0, sp = 0, spec = 0;


        for (int i = 0; i <str.length() ; i++) {
            if (Character.isUpperCase(str.charAt(i))){
                up++;
                let++;
            }
            else if(Character.isLowerCase(str.charAt(i))){
                low++;
                let++;
            }
            else if(Character.isDigit(str.charAt(i))) dig++;
            else if(Character.isWhitespace(str.charAt(i))) sp++;
            else spec++;
        }
        System.out.println("Letters = " + let);
        System.out.println("Upper = " + up);
        System.out.println("Lower = " + low);
        System.out.println("Digit = " + dig);
        System.out.println("Space = " + sp);
        System.out.println("Special = " + spec);

    }
}
