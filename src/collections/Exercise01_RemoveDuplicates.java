package collections;

import java.util.*;

public class Exercise01_RemoveDuplicates {
    public static void main(String[] args) {

        List<Integer> list1 = new ArrayList<>(Arrays.asList(2, 3, 4, 5, 5, 2));
        List<Integer> list2 = new ArrayList<>(Arrays.asList(10, 10, 10, 10));
        List<Integer> list3 = new ArrayList<>(Arrays.asList(-3, -3, -5, 0, 0 ));

        System.out.println(removeDuplicates(list1)); // [2, 3, 4, 5]
        System.out.println(removeDuplicates(list2)); // [10]
        System.out.println(removeDuplicates(list3)); // [0, -3, -5]
        System.out.println();

        System.out.println("\n----Treeset Way-----\n");
        System.out.println(removeDup(list1));// [2, 3, 4, 5]
        System.out.println(removeDup(list2));// [10]
        System.out.println(removeDup(list3));// [-5, -3, 0]
    }

    /*
    Create a method that takes some numbers and return the numbers back with all duplicates removed

    2, 3, 4, 5, 5, 2 -> 2, 3, 4, 5
    10, 10, 10, 10 -> 10
    -3, -3, -5, 0, 0 -> -3, -5, 0

    NOTE: The order of elements returned does not matter!!!
     */
    public static HashSet<Integer> removeDuplicates(List<Integer> numbers){
       // HashSet<Integer> uniques = new HashSet<>(numbers);
        // return uniques;
        return new HashSet<>(numbers);
    }
    public static TreeSet<Integer> removeDup(List<Integer> nums){
        return new TreeSet<>(nums);
    }
 }
