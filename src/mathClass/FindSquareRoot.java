package mathClass;

public class FindSquareRoot {
    public static void main(String[] args) {
        System.out.println(Math.sqrt(16)); //4
        System.out.println(Math.sqrt(-16));//gives error

        System.out.println(Math.round(5.5));
        System.out.println(Math.round(45.3));
        System.out.println(Math.round(1.499999));//1
    }
}
