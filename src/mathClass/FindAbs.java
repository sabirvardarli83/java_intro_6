package mathClass;

public class FindAbs {
    public static void main(String[] args) {
        // -35

        int num =  -100;
        System.out.println(Math.abs(num));
        //or
        System.out.println(Math.abs(-100));//converts negative to positive
    }
}
