package mathClass;

import java.util.Scanner;

public class Practice {
    public static void main(String[] args) {

        System.out.println("\n-------TASK-1-------");
        System.out.println((int)Math.random() * 5);
        System.out.println(30 * 5);

        System.out.println("\n-------TASK-2-------");
        double random1 = (Math.round (Math.random() * 9) + 1);
        double random2 = (Math.round (Math.random() * 9) + 1);

        System.out.println("Min number =" + Math.min(random1, random2));
        System.out.println("Max number =" + Math.max(random1, random2));
        System.out.println("Difference number =" + Math.abs(random1 -random2));


        System.out.println("\n-------TASK-3-------");
        int ran1 = (int) (Math.random() * 51 + 50);
        System.out.println("The random number %10 = " + ran1 % 10);

        System.out.println("\n-------TASK-4-------");

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter 5 numbers between [1-10]");
        int num1 = input.nextInt() * 5;
        int num2 = input.nextInt() * 4;
        int num3 = input.nextInt() * 3;
        int num4 = input.nextInt() * 2;
        int num5 = input.nextInt() * 1;//we dont have to write  * 1

        System.out.println(num1 + num2 + num3 + num4 + num5);

    }
}
