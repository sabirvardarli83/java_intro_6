package primitives;

public class Characters {
    public static void main(String[] args) {
        /*
        char  -> 2 bytes
        it is used to store a single character
        letter, digit, space, specials

        dataType variableName = 'value';
         */

        char c1 = 'A';
        char c2 = ' '; // empty.but when highlight can be seen

        System.out.println(c1);
        System.out.println(c2);

        char myFavCharacter = 'P';

        System.out.println("myFavCharacter = " + myFavCharacter);


    }
}
