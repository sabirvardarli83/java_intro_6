package primitives;

public class AbsoluteNumbers {
    public static void main(String[] args) {
        /*
        Absolute numbers: 1, 5, -3, -125, 2341243534
        byte -> -128 to 127 (both inclusive)
        short -> -32768 to 32767
        int -> -2147483648 to 2147483647
        long

        dataType variableName = value;
         */

        System.out.println("\n------byte-------\n");
        byte myNumber = 45;
        System.out.println(myNumber); //45

        System.out.println("The max value of byte = " + Byte.MAX_VALUE);
        System.out.println("The min value of byte = " + Byte.MIN_VALUE);

        System.out.println("\n------short-------\n");
        short numberShort = 150;
        System.out.println(numberShort);

        System.out.println("The max value of short = " + Short.MAX_VALUE); //32767
        System.out.println("The min value of short = " + Short.MIN_VALUE); //-32768

        System.out.println("\n------int-------\n");
        int myInteger = 2000000;

        System.out.println(myInteger);
        System.out.println("The max value of int = " + Integer.MAX_VALUE);
        System.out.println("The min value of int = " + Integer.MIN_VALUE);

        System.out.println("\n------long-------\n");
        long myBignumber = 2147483648L;

        System.out.println(myBignumber); //2147483648

        System.out.println("\n------long more info-------\n");
        long l1 = 5;
        long l2 = 232357097034955924L;
        System.out.println(l1);
        System.out.println(l2);










    }
}
