package utilities;

import java.util.Scanner;

public class ScannerHelper {
    static Scanner input = new Scanner(System.in);

    // Write a method that ask and return a first name from user

    public static String getFirstName(){
        System.out.println("Please enter a first name");

        return input.nextLine();
    }

    // Write a method that ask and return a last name from user

    public static String getLastName(){
        System.out.println("Please enter a last name");

        return input.nextLine();

    }
    // Write a method that ask and return an age from user
   // method name should be getAge
    // it should be static

    public static int getAge(){
        System.out.println("Please enter an age");
        int age = input.nextInt();
        input.nextLine();
        return age;
    }

    // Write a method that ask and return a number from user
    // getNumber
    // it should be static

    public static int getNumber(){
        System.out.println("Please enter a number");
        int number = input.nextInt();
        input.nextLine();
        return number;
    }

    public static String getString(){
        System.out.println("Please enter a String");
        String str = input.nextLine(); // or we can just write and delete this line. return input.nextLine();
        return str;
    }

    public static String getFavoriteBook(){
        System.out.println("Please enter your favorite book!");

        return input.nextLine();

    }
    public static String getQuote(){
        System.out.println("Please enter your favorite quote");
        return input.nextLine();
    }
    public static String getAddress(){
        System.out.println("Please enter your full address");
        return input.nextLine();
    }
    public static String getFavoriteCountry() {
        System.out.println("Please enter your favorite Country!");
        return input.nextLine();
    }
    public static String getSentence() {
        System.out.println("Please enter a sentence");
        return input.nextLine();
    }
}
