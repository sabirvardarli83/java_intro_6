package utilities;

public class MathHelper {

    // write a public static method named max() and returns the greatest number of 3 numbers
    /*
    return or void?                 -> return
    static or non-static            -> static
    what is the name of the method  -> max()
    what it returns                 -> int
    does it take arguments?         -> 3 int arguments
     */

    public static int max(int num1, int num2, int num3){
      return Math.max(Math.max(num1,num2),num3);

    }
      //  HOMEWORK

    //Write a public static method named sum() and returns the sum of 2 numbers
    //Write a public static method named product() and returns the product of 2 numbers
    //Write a public static method named square() and returns the square of a number
     public static int sum( int n1, int n2){
        return n1 + n2; // return sum(n1, n2) + n3; = return a + b + c -> we can do also like that
     }
     public static int product(int n1, int n2){
        return n1 * n2;
     }
     public static int square(int n1){
        return n1 * n1;
     }
}
