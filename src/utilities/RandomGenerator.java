package utilities;

import java.util.Random;

public class RandomGenerator {
    // write a method that generate random number between the range given and return it

    public static int getRandomNumber(int start, int end){
        Random r = new Random();
        return r.nextInt(end - start) + start;

    }
}
