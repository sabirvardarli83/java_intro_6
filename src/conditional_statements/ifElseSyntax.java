package conditional_statements;

public class ifElseSyntax {
    public static void main(String[] args) {
    /*
    -If else statements are used to control the flow of the program based on a condition
    -conditions should always either true or false
    -if block can be used without an else block
    -else statements cannot be used without if  - gives compiler error
     */

        boolean condition = true;

        if (condition){
            System.out.println("A");
        }
        else{
            System.out.println("B");
        }
        System.out.println("End of the program!");
    }
}
