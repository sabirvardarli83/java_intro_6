package conditional_statements;

import java.util.Random;

public class Practice2 {
    public static void main(String[] args) {
        /*
        Requirement:
        Write a program that generates a
        random number between 0 and 50
        (both 0 and 50 are included)
        Print true if number is in between 10
        and 25 (10 and 25 included)
        Print false otherwise
         */
        // way-1
        Random r = new Random();
        //  int number = r .nextInt(51);
        //  System.out.println(number);

        //  if (number >= 10 && number <= 25) System.out.println(true);

        //  else System.out.println(false);

        // way-2
        //   int number = (int) (Math.random() * 51);
        //   if (number >= 10 && number <= 25) System.out.println(true);

        //   else System.out.println(false);

        // way-3
        // int number = (int) (Math.random() * 51);
        //  System.out.println(number >= 10 && number <= 25);

        System.out.println("\n--------TASK-2--------\n");
        /*
        Requirement:
        Write a program that generates a random number
        between 1 and 100 (both 1 and 100 are included)
        Find which quarter and half is number in
        1st quarter is 1-25
        2nd quarter is 26-50
        3rd quarter is 51-75
        4th quarter is 76-100
        1st half is 1-50
        2nd half is 51-100
        Test data:
        34
        Expected result:
        34 is in the 1st half
        34 is in the 2nd quarter (edited)
        */
        //  int x = r.nextInt(100) + 1;
        //  System.out.println(x);

        //  if (x >= 1 && x<=25){
        //      System.out.println(x + " is in the 1st quarter");
        //      System.out.println(x + " is in the 1st half");
        //  }
        //  else if (x >= 26 && x<= 50){
        //      System.out.println(x + " is in the 2nd quarter");
        //      System.out.println(x + " is in the 1st half");
        //  }
        //  else if (x >= 51 && x<= 75) {
        //      System.out.println(x + " is in the 3rd quarter");
        //      System.out.println(x + " is in the 2nd half");
        //  }
        //  else {
        //      System.out.println(x + " is in the 4th quarter");
        //      System.out.println(x + " is in the 2nd half");

        System.out.println("\n--------TASK-3--------\n");
        /*
        Requirement:
        -Assume you are given a single character. (It will be hard-coded)
        -If given char is a letter, then print “Character is a letter”
        -If given char is a digit, then print “Character is a digit”
        USE ASCII TABLE for this task
        Test data:
        ‘v’
        Expected result:
        Character is a letter
        Test data:
        ‘5’
        Expected result:
        Character is a digit

         */
       char c = 'v';

       if ((c >= 65 && c <= 90) || (c >= 97 && c<= 122)) System.out.println("Character is a letter");
       else if (c >= 48 && c <= 57) System.out.println("Character is a digit");




    }



    }

