package conditional_statements;

import java.util.Scanner;

public class SwitchPractice {
    public static void main(String[] args) {
        // print out months of the year
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number from 1 to 6");
        int num = input.nextInt();

        if (num == 1) System.out.println("Jan");
        else if (num == 2) System.out.println("Feb");
        else if (num == 3) System.out.println("March");
        else if (num == 4) System.out.println("April");
        else if (num == 5) System.out.println("May");
        else if (num == 6) System.out.println("June");
        else System.out.println("Invalid number");

        switch(num){
            case 1:
                System.out.println("Jan");
                break;
            case 2:
                System.out.println("Feb");
                break;
            case 3:
                System.out.println("March");
                break;
            case 4:
                System.out.println("April");
                break;
            case 5:
                System.out.println("May");
                break;
            case 6:
                System.out.println("June");
                break;
            default:
                System.out.println("Invalid number");
        }
        System.out.println("Please enter either a, b or c");
        input.nextLine();
        String letter = input.nextLine();

        switch (letter){
            case "a":
                System.out.println("The letter you entered is a");
                break;
            case "b":
                System.out.println("The letter you entered is b");
                break;
            case "c":
                System.out.println("The letter you entered is c");
                break;
            default:
                System.out.println("Invalid letter");
        }

    }
}
