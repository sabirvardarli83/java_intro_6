package conditional_statements;

import java.util.Scanner;

public class Exercise05_CheckAllEven {
    public static void main(String[] args) {
       /*
Write a Java program that asks user to enter 3 numbers
Print true if all of them are even numbers
Otherwise, print false

EXAMPLE PROGRAM 1
Program: Please enter 3 numbers?
User: 2 4 6
Program: true


EXAMPLE PROGRAM 2
Program: Please enter 3 numbers?
User: 10 20 25
Program: false


EXAMPLE PROGRAM 3
Program: Please enter 3 numbers?
User: 1 3 5
Program: false
 */
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 3 numbers");
        int x = input.nextInt();
        int y = input.nextInt();
        int z = input.nextInt();
        if (x % 2 == 0 && y % 2 == 0 && z % 2 == 0){  // or System.out.println(x % 2 == 0 && y % 2 == 0 && z % 2 == 0); gives true or false also just 1 statement
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }
        System.out.println("End of the program");
    }




}
