package conditional_statements;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Exercise03_PosNegZero {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a number");
        int number = input.nextInt();

        //first way
        if (number > 0){
            System.out.println("POSITIVE");
        }
        else if (number < 0){
            System.out.println("NEGATIVE");
        }
        else{
            System.out.println("ZERO");
        }
        System.out.println("End of the program");

        //second way-NESTED IF STATEMENTS
        if (number > 0){
            System.out.println("POSITIVE");
        }
        else{
            //negative or zero
            if (number < 0){
                System.out.println("NEGATIVE");
            }
            else{
                System.out.println("ZERO");
            }
        }
        System.out.println("End of the program");
    }
}
