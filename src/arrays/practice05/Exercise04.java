package arrays.practice05;

public class Exercise04 {
    public static void main(String[] args) {
        /*
        Requirement:
        Write a program to find if String array contains “apple”
        as an element, ignore cases.
        Test data 1:
        String[] list = {“banana”, “orange”, “Apple”};
        Expected output:
        true
        Test data 2:
        String[] list = {“pineapple”, “banana”, “orange”,
        “grapes”};
        Expected output:
        false
        NOTE: Make your code dynamic that works for any
         */
        String[] arr = {"pineapple", "banana", "orange", "apple"};
        System.out.println(containsApple(arr));
    }
    public static boolean containsApple(String[] arr){
        boolean containApple = false;
        for (String s : arr){
            if (s.equalsIgnoreCase("apple")){
                containApple = true;
                break;
            }
        }
        return containApple;

    }
}
