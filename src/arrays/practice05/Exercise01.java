package arrays.practice05;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        Write a program to find the first positive and negative
        numbers in an int array
        Test data:
        int[] numbers = {0, -4, -7, 0, 5, 10, 45};
        Expected output:
        First positive number is: 5
        First negative number is: -4
        NOTE: Make your code dynamic that works for any
        given int array.

         */
        int[] numbers = {0, -4, -7, 0, 5, 10, 45};
        for (int number : numbers){
            if ((number > 0)){
                System.out.println("First positive number : " + number);
                break;
            }
        }
        for (int number : numbers) {
            if ((number < 0)) {
                System.out.println("First negative number : " + number);
                break;
            }
        }

        //2nd way
    }
}
