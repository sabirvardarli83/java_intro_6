package arrays;

import java.util.Arrays;

public class Exercise01_CountNumbers {
    public static void main(String[] args) {
        // Write a program that counts how many negative numbers you in the array -> 2
        System.out.println("\n-------for each loop------\n");
        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};
        int negatives = 0;
        for (int number : numbers) {
            if(number < 0)   negatives++;
        }

            System.out.println(negatives);

        System.out.println("\n-------for loop------\n");
        negatives = 0;
        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] < 0) negatives++;

        }
        System.out.println(negatives);

        // write a program that counts how many even numbers you have in the array -> 6

        int evens = 0;
        for(int number : numbers){
            if(number % 2 == 0) evens++;
        }
        System.out.println(evens);

        //write a program that finds the sum of all the numbers in the array -> 28
        //-1, 3, 0, 5, -7, 10, 8, 0, 10, 0

        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        System.out.println(sum);
    }
}
