package arrays;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {
        // 1- create an array to store -> 5.5,6,10.3,25
        double[] numbers = {5.5, 6, 10.3, 25};

        // 2- Print the arrays [5.5, 6.0, 10.3, 25.0]
        System.out.println(Arrays.toString(numbers));

        // 3- Print the size of the array -> the length is 4
            System.out.println("The length is " + numbers.length);

            // 4- print each element using for each loop
        /*
        5.5
        6.0
        10.3
        25.0
         */
        for (double element : numbers){
            System.out.println(element);
        }


    }
}
