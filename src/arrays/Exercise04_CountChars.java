package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String
        Count how many chars in the String Are letters
        "abc1234" -> 3
        "       " -> 0
        "Hello"   -> 5
         */
        String str = ScannerHelper.getString();
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
           if(Character.isLetter(str.charAt(i))) count++;
        }
        System.out.println(count);

        System.out.println("\n-------2nd way------\n");

        str = ScannerHelper.getString();
        count = 0;
        char[] chars = str.toCharArray();
        for (char c : chars){
            if ((Character.isLetter(c))) count++;
        }
        System.out.println(count);
    }
}
