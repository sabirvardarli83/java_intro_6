package arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {
        String[] cities = {"Chicago", "Miami", "Toronto"};
        // The number elements in the array

       int sizeOfTheArray = cities.length;
        System.out.println(sizeOfTheArray);

        //Get particular element from the array
        //Miami

        System.out.println(cities[1]);
        System.out.println(cities[0]);
        System.out.println(cities[2]);

        //Index out of bounds exception

        //System.out.println(cities[-1]);

        //How to print Array
        System.out.println(Arrays.toString(cities));

        System.out.println("\n-----for loop-----\n");
        for (int i = 0; i < cities.length; i++) { //if we add more cities,cities length will be better
            System.out.println(cities[i]);
        }

        System.out.println("\n-----for each loop - enhanced loop-----\n");
        //This way is better
        for(String element : cities){
            System.out.println(element);
        }







    }
}
