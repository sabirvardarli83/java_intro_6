package arrays;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.jar.JarOutputStream;

public class Exercise05_CountWords {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String
        And count how many words you have in the given String
        "Hello World" -> 2
        "Java is fun" -> 3
        "Today is a nice class with technical issues" -> 8
         */
        System.out.println("\n--------1st way------\n");
        String str = ScannerHelper.getSentence();
        int count = 0;

        for (char c : str.toCharArray()) {
           if (Character.isWhitespace(c)) count++;
        }
        System.out.println(count + 1);// wrong the bottom one true


        System.out.println("\n------2nd way------\n");
        String str2 = ScannerHelper.getString();

        System.out.println(str2.split(" ").length);//str2.split(" ") gives us an Array- with length it gives us number of words.


        System.out.println("\n---How to avoid miscounting when there is more spaces-----\n");

        String str3 = "Hello   World";//2 words
        System.out.println(str3.length());//13
        System.out.println(str3.split(" ").length);//4-wrong
        String[] arr = str3.split(" ");
        System.out.println(Arrays.toString(arr));// [Hello, , , World]
        int countWords = 0;
        for (String s : arr) {
            if (!s.isEmpty()) countWords++;

        }
        System.out.println(countWords);


    }
}
