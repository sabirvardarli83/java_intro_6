package arrays;

import java.util.Arrays;

public class IntArray {
    public static void main(String[] args) {
        //create an int array that will store 6 numbers
         int[] numbers = new int[6];//creates 6 empty cells behind the scene
        //Print the array

        Arrays.toString(numbers);
        System.out.println(Arrays.toString(numbers));//[0, 0, 0, 0, 0, 0]
        // if you print just as numbers you get result as 'location'.

        //HOW TO ASSIGN A VALUE TO AN EXISTING VALUE according to 6 numbers
        // index of 0 -> 5
        // index of 2 -> 15
        // index of 4 -> 25
        numbers[0] = 5;
        numbers[2] = 15;
        numbers[4] = 25;
        //numbers[7] = 45;//ArrayIndexOutOfBoundException

        System.out.println(Arrays.toString(numbers));//updated [5, 0, 15, 0, 25, 0]

        //Print each element for loop
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }
        for (int element : numbers){
            System.out.println(element);
        }


        //Print each element for each loop

    }
}
