package arrays;

import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};
        /*
        Check the collection you have above and print true if it contains Mouse
        Print false otherwise

        RESULT:
        true
        */
        System.out.println("\n-----------Loop way------------\n");

        boolean result = true;

        for (String object : objects){
            if(object.equals("Mouse")){
                break;
            }
        }
        System.out.println(result);

        System.out.println("\n-----------Binary Search way------------\n");

        Arrays.sort(objects);// [Keyboards, Mouse, Mouse, Remote, iPad
        System.out.println(Arrays.binarySearch(objects, "Mouse") >= 0);//true
        System.out.println(Arrays.binarySearch(objects, "mouse") >= 0);//false

    }
}
