package recursion;

public class TestRecursion {
    public static void main(String[] args) {
        System.out.println(sum1ToNIterative(5));// 15
        System.out.println(sum1ToNIterative(10));// 55
        System.out.println();

        System.out.println(sum1ToNRecursive(5));//15
        System.out.println(sum1ToNRecursive(10));//55
        System.out.println();

        System.out.println(factorialRecursive(5)); //120
        System.out.println(factorialRecursive(4)); //24

    }

    public static int sum1ToNIterative(int n){
        int sum = 0;

        for (int i = 0; i <= n; i++) {
            sum += i;
        }
        return sum;
    }

    public static int sum1ToNRecursive(int n){
        if (n != 1) return n + sum1ToNRecursive(n-1);
        return 1;
    }

    /*
    factorialRecursive(3)       -> 1 * 2 * 3
    factorialRecursive(2)       -> 1 * 2
    factorialRecursive(1)       -
     */
    public static int factorialRecursive(int n){
    if (n != 1) return n * factorialRecursive(n-1);
    return 1;
    }


}
