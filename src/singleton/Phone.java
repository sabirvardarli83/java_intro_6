package singleton;

public class Phone {

    // instance variable which is also Phone
    public static Phone phone; // // singleton.Phone@1540e19d

    private Phone(){
    }

    // Create a method that instantiate a Phone object and return it
    public static Phone getPhone(){
       if (phone == null) phone = new Phone();
        return phone;
    }
}
