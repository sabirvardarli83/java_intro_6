package singleton;

public class TestSingleton {
    public static void main(String[] args) {
        Phone p1 = Phone.getPhone();
        Phone p2 = Phone.getPhone();
        Phone p3 = Phone.getPhone();

        System.out.println(p1); // singleton.Phone@1540e19d
        System.out.println(p2); // singleton.Phone@1540e19d
        System.out.println(p3); // singleton.Phone@1540e19d


        Driver d1 = Driver.getDriver();
        Driver d2 = Driver.getDriver();
        System.out.println(d1); // singleton.Driver@677327b6
        System.out.println(d2);// singleton.Driver@677327b6
    }
}
