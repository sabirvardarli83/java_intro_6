package operators.increment_decrement_operators;

public class PostIncrementPostDecrement {
    public static void main(String[] args) {
        int num1 = 10, num2 = 10;

        System.out.println(num1++);
        System.out.println(num1);
        System.out.println(++num2);

        System.out.println("\n-------TASK-2-------");
        int n1 = 5, n2 = 7;

        n1++;// keep it as 5 but increase it by 1 for the next use
        n1 += n2;

        System.out.println(n1);//13

        System.out.println("\n-------TASK-3-------");

        int i1 = 10;

        --i1;//decrease it by 1 right now-9
        i1--;//decrease it by one for the next use

        System.out.println(--i1);

        System.out.println("\n-------TASK-4-------");

        int number1 = 50;

        number1 -= 25;
        number1 -= 10;

        System.out.println(number1--);//

        System.out.println("\n-------TASK-5-------");

        int i = 5;

        int age = 10 * i++;
        System.out.println(age); //50

        System.out.println("\n-------TASK-6-------");

        int var1 = 27;
        int result = --var1 / 2;
        System.out.println(++result);





    }
}
