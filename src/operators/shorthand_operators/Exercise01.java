package operators.shorthand_operators;

public class Exercise01 {
    public static void main(String[] args) {
        int a = 8;
        int b = 3;
        /*
        use a and b to practice
         */

        System.out.println(a += b); //bu yontem de dogru
        System.out.println(a -= b);
        System.out.println(a *= b);
        System.out.println(a /= b);
        System.out.println(a %= b);


        System.out.println("\n------Task-1------\n");
        a += b;
        System.out.println(a);
        System.out.println(b);
        System.out.println("\n------Task-2------\n");
        a -= b;
        System.out.println(a);
        System.out.println(b);
        System.out.println("\n------Task-3------\n");
        a *= b;
        System.out.println(a);
        System.out.println(b);
        System.out.println("\n------Task-4------\n");
        a /= b;
        System.out.println(a);
        System.out.println(b);
        System.out.println("\n------Task-5------\n");
        a %= b;
        System.out.println(a);
        System.out.println(b);


    }
}
