package operators.shorthand_operators;

public class ShorthandPractice {
    public static void main(String[] args) {

        int age = 45; // in 2023

        System.out.println("Age in 2023 = " + age);

        //What will be the age in 2028-5 years later
        //age = age + 5;
        age += 5; // increase the age by 5 =shorthand

        System.out.println(age); // 50

        age -= 20;//decrease by 20
        System.out.println(age);//30

        age /= 3;//divide by 3
        System.out.println(age);//10

        age %= 4;//remainder
        System.out.println(age);//2

    }
}
