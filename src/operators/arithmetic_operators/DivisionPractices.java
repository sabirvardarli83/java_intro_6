package operators.arithmetic_operators;

public class DivisionPractices {
    public static void main(String[] args) {
        /*
        5 / 2 = 2.5

        int 5 / int 2 = 2  int kullaninca  tam sayiyi aliyor.

        double 5 / double 2 = 2.5

         */

        int i1 = 5, i2 = 2;
        double d1 = 5, d2 = 2;

        int div1 = i1 / i2;
        double div2 = d1 / d2;

        System.out.println(div1);
        System.out.println(div2);

        /*
            TASK
        Divide 15 by 2

        Expected output:
        The division of 15 by 2 = 7.5
        */

        double d3 = 15, d4 = 2;

        System.out.println("The division of " + d3 + " by " + d4 + " = " + d3 / d4);


    }
}
