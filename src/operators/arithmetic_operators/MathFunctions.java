package operators.arithmetic_operators;

public class MathFunctions {
    public static void main(String[] args) {
        /*
        Find the
        sum             ->12
        multiplication  ->27
        subtraction     ->6
        division        ->3
        remainder       ->0

         */

        int num1 = 9, num2 = 3;

        int sum = num1 + num2;
        System.out.println(sum);

        int mul = num1 * num2;
        System.out.println(mul);

        int sub = num1 - num2;
        System.out.println(sub);

        int div = num1 / num2;
        System.out.println(div);

        int rem = num1 % num2;
        System.out.println(rem);

    }
}
