package operators.arithmetic_operators;

public class AdditionSubtractionConcatenation {
    public static void main(String[] args) {
        /*
        Find sum, and subtraction of 10 and 5
        sum = 15
        subtraction = 5
        Expected Output:
        The sum = 15
        The subtraction = 5
         */

        System.out.println("The sum = " + (10 + 5) );//only plus and subtraction needs extra parenthesis.remainder,multiply and division no need extra parenthesis
        System.out.println(5 + 10 + " Hello" + 5 + 10); //15 Hello510

        System.out.println("The Sub = " + (10 - 5) );

        System.out.println("The remainder = " + 10 % 5 );
        System.out.println("The product = " + 10 * 5 );
        System.out.println("The division = " + 10 / 5 );


    }
}
