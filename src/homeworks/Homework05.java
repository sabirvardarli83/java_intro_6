package homeworks;

import utilities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------\n");
        String solution = "";
        for(int i = 1; i <= 100; i++){
            if(i % 7 == 0) solution += i + " - ";
        }
        System.out.println(solution.substring(0,solution.length() - 3));

        System.out.println("\n----------TASK-2----------\n");
        solution = "";
        for(int i = 1; i <= 50; i++){
            if(i % 6 == 0) solution += i + " - ";
        }
        System.out.println(solution.substring(0,solution.length() - 3));

        System.out.println("\n----------TASK-3----------\n");
        solution = "";
        for(int i = 100; i >= 50; i--){
            if(i % 5 == 0) solution += i + " - ";
        }
        System.out.println(solution.substring(0,solution.length() - 3));

        System.out.println("\n----------TASK-4----------\n");

        for(int i = 0; i <= 7; i++){
            System.out.println("The square of " + i + " is = " + i * i);
        }

        System.out.println("\n----------TASK-5----------\n");
        int sum = 0;
        for(int i = 1; i<= 10; i++){
         sum += i;
        }
        System.out.println(sum);

        System.out.println("\n----------TASK-6----------\n");

        int num = ScannerHelper.getNumber();
        int factorial = 1;
        int i = 1;
        while(i <= num){
            factorial *= i;
            i++;
        }
        System.out.println(factorial);

        System.out.println("\n----------TASK-7----------\n");

        String name = ScannerHelper.getFirstName().toLowerCase();
        int count = 0;
        for(int j = 0; j <= name.length() - 1; j++){
            if(name.charAt(j) == 'a' || name.charAt(j) == 'e' || name.charAt(j) == 'i' || name.charAt(j) == 'u' ||
                    name.charAt(j) == 'o') count++;
        }
        System.out.println("There are " + count + " vowel letters in this full name");

        System.out.println("\n----------TASK-8----------\n");

        String aName;
        do{
            aName = ScannerHelper.getFirstName().toLowerCase();
        }
        while(aName.charAt(0) != 'j');
        System.out.println("End of the program");
    }
}
