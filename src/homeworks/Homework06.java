package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {
        System.out.println("\n--------TASK-1--------\n");
        int[] numbers = new int[10];
        numbers[2] = 23; numbers[4] = 12; numbers[7] = 34; numbers[9] = 7; numbers[6] = 15;
        numbers[0] = 89;
        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n--------TASK-2--------\n");
        String[] words = new String[5];
        words[1] = "abc";
        words[4] = "xyz";
        System.out.println(words[3]);//null
        System.out.println(words[0]);//null
        System.out.println(words[4]);//xyz
        System.out.println(Arrays.toString(words));

        System.out.println("\n--------TASK-3--------\n");
        int[] nums = {23, -34, -56, 0, 89, 100};
        System.out.println(Arrays.toString(nums));
        Arrays.sort(nums);
        System.out.println(Arrays.toString(nums));//sorted

        System.out.println("\n--------TASK-4--------\n");
        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};
        System.out.println(Arrays.toString(countries));
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));//sorted

        System.out.println("\n--------TASK-5--------\n");
        String[] cartoons = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};
        System.out.println(Arrays.toString(cartoons));
        for (String element : cartoons){
            if (element.equals("Pluto")) System.out.println(true);
            else System.out.println(false);
        }
        System.out.println("\n--------TASK-6--------\n");
        String[] catCartoons = {"Garfield", "Tom", "Sylvester", "Azrael"};
        Arrays.sort(catCartoons);
        System.out.println(Arrays.toString(catCartoons));
        for (String cat : catCartoons){
            if (cat.contains("Garfield") && cat.contains("Felix"))
                System.out.println(true);
            else System.out.println(false);
        }

        System.out.println("\n--------TASK-7--------\n");
        double[] balances = {10.5, 20.75, 70.0, 80.0, 15.75};
        System.out.println(Arrays.toString(balances));
      for (double number : balances){
          System.out.println(number);
      }
        System.out.println("\n--------TASK-8--------\n");
      char[] c = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        System.out.println(Arrays.toString(c));
        int letter = 0;
        int  lower = 0;
        int upper = 0;
        int digit = 0;
        int specialC = 0;

        for (int i = 0; i < 11; i++) {
            if (Character.isLetter(c[i])) letter++;
            else if (Character.isLowerCase(c[i])) lower++;
            else if (Character.isUpperCase(c[i])) upper++;
            else if (Character.isDigit(c[i])) digit++;
            else specialC++;
        }
        System.out.println("Letters = " + letter);
        System.out.println("Uppercase letters = " + upper);
        System.out.println("Lowercase letters = " + lower);
        System.out.println("Digits = " + digit);
        System.out.println("Special characters = " + specialC);

        System.out.println("\n--------TASK-9--------\n");
        String[] strings = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        System.out.println(Arrays.toString(strings));
        int up = 0;
        int low = 0;
        int bP = 0;
        int bookPen = 0;
      for (String str : strings){
          if (Character.isUpperCase(str.charAt(0))) up++;
          else if (Character.isLowerCase(str.charAt(0))) low++;
          else if (str.startsWith("B") || str.startsWith("P")) bP++;
          else if (str.equals("book") || str.equals("pen")) bookPen++;
      }
        System.out.println("Elements starts with uppercase = " + up);
        System.out.println("Elements starts with lowercase = " + low);
        System.out.println("Elements starting with B or P = " + bP);
        System.out.println("Elements having book or pen  = " + bookPen);

        System.out.println("\n--------TASK-10--------\n");
        int[] values = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        System.out.println(Arrays.toString(values));
        int b10 = 0;
        int l10 = 0;
        int e10 = 0;
        for (int num1 : values){
            if (num1 > 10) b10++;
            else if (num1 < 10) l10++;
            else e10++;
        }
        System.out.println("Elements that are more than 10 = " + b10);
        System.out.println("Elements that are less than 10 = " + l10);
        System.out.println("Elements that are 10 = " + e10);

        System.out.println("\n--------TASK-11--------\n");
        int[] first = {5, 8, 13, 1, 2};
        int[] second = {9, 3, 67, 1, 0};
        int[] third = {9, 8, 67, 1, 2};
        System.out.println("1st array is = " + Arrays.toString(first));
        System.out.println("2nd array is = " + Arrays.toString(second));
        System.out.println("3rd array is = " + Arrays.toString(third));
        /*
        int[][] greatest = {{5, 8, 13, 1, 2}, {9, 3, 67, 1, 0}};
        int a = Math.max(greatest[0][0], greatest[1][0]);
        int b = Math.max(greatest[0][1], greatest[1][1]);
        int d = Math.max(greatest[0][2], greatest[1][2]);
        int e = Math.max(greatest[0][3], greatest[1][3]);
        int f = Math.max(greatest[0][4], greatest[1][4]);
        */

    }
}
