package homeworks;

import java.sql.SQLOutput;

public class Homework01 {
    public static void main(String[] args) {
        System.out.println("\n---------TASK1---------\n");
        /*
        JAVA ;
        J = 74 , A = 65 , V = 86 , A = 65
        01001010 01000001 01010110 01000001
        SELENIUM ;
        S = 83 , E = 69 , L = 76 , E = 69 , N = 78 , I = 73 , U = 85 , M = 77
        01010011 01000101 01001100 01000101 01001110 01001001 01010101 01001101
        */

        System.out.println("\n---------TASK2---------\n");
        /*
        01001101 = 77 = M
        01101000 = 104 = h
        01111001 = 121 = y
        01010011 = 83 = S
        01101100 = 108 = l
         */

        System.out.println("\n---------TASK3---------\n");

        System.out.println("I start to practice \"JAVA\" today, and I like it."); //I start to practice "JAVA" today, and I like it.
        System.out.println("The secret of getting ahead is getting started.");//The secret of getting ahead is getting started.
        System.out.println("\"Don't limit yourself. \"");//"Don't limit yourself. "
        System.out.println("Invest in your dreams. Grind now. Shine later.");//Invest in your dreams. Grind now. Shine later.
        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.");//It’s not the load that breaks you down, it’s the way you carry it.
        System.out.println("The hard days are what make you stronger.");//The hard days are what make you stronger.
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.");//You can waste your lives drawing lines. Or you can live your life crossing them.

        System.out.println("\n---------TASK4---------\n");

        System.out.println("\tJava is easy to write and easy to run—this is the foundational \nstrength of Java and why many developers program in it. When you \nwrite Java once, you can run it almost anywhere at any time.\n" +
                "\n" +
                "\tJava can be used to create complete applications that can run on \na single computer or be distributed across servers and clients in a \nnetwork.\n" +
                "\n" +
                "\tAs a result, you can use it to easily build mobile applications or \nrun-on desktop applications that use different operating systems and \nservers, such as Linux or Windows.\n");


        System.out.println("\n---------TASK5---------\n");

     int myAge = 39;
        System.out.println(myAge);//myAge

     int myFavoriteNumber = 2018;
        System.out.println(myFavoriteNumber);//myFavoriteNumber

     double myHeight = 5.97;
        System.out.println(myHeight);//myHeight

     double myWeight = 228.4;
        System.out.println(myWeight);//myWeight

      char myFavoriteLetter = 'Z';
        System.out.println(myFavoriteLetter);//myFavoriteLetter








    }
}
