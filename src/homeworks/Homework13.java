package homeworks;

import com.sun.xml.internal.bind.v2.runtime.output.StAXExStreamWriterOutput;

import java.sql.Array;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;

public class Homework13 {
    public static void main(String[] args) {
        System.out.println("\n-----------TASK-1--------\n");
        System.out.println(hasLowerCase("hello")); // true
        System.out.println(hasLowerCase("JAVA")); // false
        System.out.println(hasLowerCase("123$")); // false

        System.out.println("\n-----------TASK-2--------\n");
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1, 0, 0)))); // [1]
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0, 6, 8)))); // [6, 8]

        System.out.println("\n-----------TASK-3--------\n");
        int[] array = {0,3,6};
        int[][] result = numberAndSquare(array);
        System.out.println(Arrays.deepToString(result));

        System.out.println("\n-----------TASK-4--------\n");
        String[] words = {"abc", "foo", "java"};
        String str = "hello";
        System.out.println(containsValue(words, str)); // false

        String[] words2 = {"abc", "def", "123", "Java", "Hello"};
        String str2 = "123";
        System.out.println(containsValue(words2, str2)); // true

        System.out.println("\n-----------TASK-5--------\n");
        String sentence = "Java is fun";

        System.out.println(reverseSentence(sentence)); // Fun Is Java

        System.out.println("\n-----------TASK-6--------\n");
        System.out.println(removeStringSpecialsDigits("123Java #$%is fun")); // Java is fun

        System.out.println("\n-----------TASK-7--------\n");
       String[] arrays = {"123Java", "#$%is", "fun"};
        System.out.println(Arrays.toString(removeArraySpecialsDigits(arrays))); //[Java, is, fun]

        System.out.println("\n-----------TASK-8--------\n");
        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("Java", "is", "fun"));
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("Java", "C#", "Python"));
        System.out.println(removeAndReturnCommons(list1, list2)); // [Java]

    }




    public static boolean hasLowerCase(String str){

        for (char c : str.toCharArray()) {
            if (Character.isLowerCase(c))  return true;
        }
        return false;
    }
    public static ArrayList<Integer> noZero(ArrayList<Integer> list){
    ArrayList<Integer> withoutZeros = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
           if (list.get(i) != 0){
               withoutZeros.add(list.get(i));
           }
        }
        return withoutZeros;
    }
    public static int[][] numberAndSquare(int[] nums){
        int[][] numbers = new int[nums.length][2];
        for (int i = 0; i < nums.length; i++) {
            int number = nums[i];
            int square = nums[i] * nums[i];
            numbers[i] = new int[]{number, square};
        }
        return numbers;
    }

    public static boolean containsValue(String[] words, String str){


        for (String word : words) {
            if (word.equals(str)){
                return true;
            }
        }
        return false;

    }

    public static String reverseSentence(String str){
        String[] words = str.split(" ");
        if (words.length < 2) {
            return "There is not enough words!";
        }
        StringBuilder reversed = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            String word = words[i].toLowerCase();
            word = Character.toUpperCase(word.charAt(0)) + word.substring(1);
            reversed.append(word).append(" ");
        }
        return reversed.toString().trim();
    }
    public static String removeStringSpecialsDigits(String str){
        String result = "";
        for (char c : str.toCharArray()) {
            if (Character.isLetter(c) || c == ' '){
                result += c;
            }
        }
        return result;
    }
    public static String[] removeArraySpecialsDigits(String[] array) {
        String[] results = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            String input = array[i];
            String regex = "[^a-zA-Z]";

            input = input.replaceAll(regex, "");
            results[i] = input;
        }
        return results;
    }

    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> list1, ArrayList<String> list2){
     ArrayList<String> commons = new ArrayList<>();
        for (String s : list1) {
            if (list2.contains(s)){
                commons.add(s);
            }
        }
        return commons;
    }



}
