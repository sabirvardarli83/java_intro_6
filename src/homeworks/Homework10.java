package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Homework10 {
    public static void main(String[] args) {
        System.out.println("\n--------TASK-1-----\n");
        String str = "     Java is fun       ";
        String str2 = "Selenium is the most common UI automation tool.   ";
        String str3 = "Hello hi    how   are     you  my friend?";
        System.out.println(countWords(str));// 3
        System.out.println(countWords(str2));// 8
        System.out.println(countWords(str3));// 7

        System.out.println("\n--------TASK-2-----\n");
        String str4 = "TechGlobal is a QA bootcamp";
        String str5 = "QA stands for Quality Assurance";
        System.out.println(countA(str4)); // 4
        System.out.println(countA(str5)); // 5

        System.out.println("\n--------TASK-3-----\n");
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67)))); // 3
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123)))); // 4

        System.out.println("\n--------TASK-4-----\n");
        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60)))); // [35, 20, 70, 10, 60]
        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(1, 2, 5, 2, 3)))); // [1, 2, 3, 5]

        System.out.println("\n--------TASK-5-----\n");
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#","ruby","JAVA","ruby","C#","C++"))));// [C#, JAVA, C++, java, ruby]

        System.out.println("\n--------TASK-6-----\n");
        String str6 = "  I   am      learning     Java      ";
        String str7 = "Java  is fun    ";
        System.out.println(removeExtraSpaces(str6));// I am learning Java
        System.out.println(removeExtraSpaces(str7)); // Java is fun

        System.out.println("\n--------TASK-7-----\n");
        int[] arr1 = {3, 0, 0, 7, 5, 10};
        int[] arr2 = {6, 3, 2};
        System.out.println(Arrays.toString(add(arr1, arr2))); // [9, 3, 2, 7, 5, 10]

        System.out.println("\n--------TASK-8-----\n");
        int[] nums = {10, -13, 8, 12, 15, -20};
        System.out.println(findClosesTo10(nums)); // 8








    }


    public static int countWords(String str) {
        return str.trim().split("\\s+").length;
    }

    public static int countA(String str) {
        return str.replaceAll("[^Aa]", "").length();
    }

    public static int countPos(ArrayList<Integer> numbers) {
        int count = 0;
        for (Integer number : numbers) {
            if (number > 0) count++;
        }
        return count;
    }

    public static HashSet<Integer> removeDuplicateNumbers(ArrayList<Integer> nums) {
        return new HashSet<>(nums);
    }

    public static HashSet<String> removeDuplicateElements(ArrayList<String> list) {
        return new HashSet<>(list);
    }

    public static String removeExtraSpaces(String str){

        return str.replaceAll("\\s+", " ").trim();
    }

    public static int[] add(int[] num1, int[] num2){
        for (int i = 0; i < Math.min(num1.length, num2.length); i++) {
            if (num1.length > num2.length) num1[i] += num2[i];
            else num2[i] += num1[i];
        }
        return num1.length > num2.length ? num1 : num2;

    }

    public static int findClosesTo10(int[] numbers){
        Arrays.sort(numbers);
        int closest = numbers[0];
        for (int number : numbers) {
            if (number != 10 && Math.abs(number - 10) < Math.abs(closest - 10)) closest = number;

        }
        return closest;
    }
}
