package homeworks;

public class Homework14 {
    public static void main(String[] args) {
        System.out.println("\n--------TASK-1------\n");
        fizzBuzz1(18);

        System.out.println("\n--------TASK-2------\n");
        System.out.println(fizzBuzz2(15));// FizzBuzz
        System.out.println(fizzBuzz2(13));// 13
        System.out.println(fizzBuzz2(5));// Buzz

        System.out.println("\n--------TASK-3------\n");
        System.out.println(findSumNumbers("ab110c045d")); // 11
        System.out.println(findSumNumbers("525")); // 12
        System.out.println(findSumNumbers("abc$")); // 0

        System.out.println("\n--------TASK-4------\n");

        System.out.println("\n--------TASK-5------\n");
        System.out.println(countSequenceOfCharacters("abc"));

    }
    public static void fizzBuzz1(int num){
        for(int i = 1; i <= num; i++){
            if(i % 15 == 0) System.out.println("FizzBuzz");
            else if(i % 5 == 0) System.out.println("Buzz");
            else if(i % 3 == 0 ) System.out.println("Fizz");
            else System.out.println(i);
        }
    }
    public static String fizzBuzz2(int num){

            if (num % 15 == 0) return "FizzBuzz";
            else if (num % 5 == 0) return "Buzz";
            else if (num % 3 == 0) return "Fizz";
            else return String.valueOf(num);
    }

    public static int findSumNumbers(String str){
        String digits = str.replaceAll("[^0-9]", "");
        int sum = 0;
       if (!digits.isEmpty()){
           for (char c : digits.toCharArray() ) {
               int digit = Character.getNumericValue(c);
               sum += digit;
           }
       }
       return sum;
    }
 //  public static int findBiggestNumber(String str){
 //      String digits = str.replaceAll("[^0-9]", "");
 //      int biggest = Integer.MIN_VALUE;
 //  }

    public static String countSequenceOfCharacters(String input){
        if (input.isEmpty()) {
            return "";
        }

        String countStr = "";
        int count;
        char currentChar, nextChar;

        for (int i = 0; i < input.length(); i++) {
            count = 1;
            currentChar = input.charAt(i);

            for (int j = i + 1; j < input.length(); j++) {
                nextChar = input.charAt(j);

                if (nextChar == currentChar) {
                    count++;
                } else {
                    break;
                }
            }

            countStr += count + "" + currentChar;
            i += count - 1;
        }

        return countStr;
    }
}
