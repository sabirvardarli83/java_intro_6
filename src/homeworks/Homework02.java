package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n--------TASK-1--------\n");

        int number1, number2;
        System.out.println("Please enter your number 1 ?");
        number1 = input.nextInt();

        System.out.println("Please enter your number 2 ?");
        number2 = input.nextInt();

        System.out.println("The sum of the numbers you entered is " + (number1 + number2));

        System.out.println("\n--------TASK-2--------\n");

        int num1, num2;
        System.out.println("Please enter your 1st number?");
        num1 = input.nextInt();

        System.out.println("Please enter your 2nd number?");
        num2 = input.nextInt();

        System.out.println("The product of the given 2 numbers is: " +  (num1 * num2) );

        System.out.println("\n--------TASK-3--------\n");

        double myNumber1, myNumber2;
        System.out.println("Please enter your first number?");
        myNumber1 = input.nextDouble();
        System.out.println("Please enter your second number?");
        myNumber2 = input.nextDouble();

        System.out.println("The sum of the given number is = " + (myNumber1 + myNumber2));
        System.out.println("The product of the given number is = " + (myNumber1 * myNumber2));
        System.out.println("The subtraction of the given number is = " + (myNumber1 - myNumber2));
        System.out.println("The division of the given number is = " + (myNumber1 / myNumber2));
        System.out.println("The remainder of the given number is = " + (myNumber1 % myNumber2));

        System.out.println("\n--------TASK-4--------\n");
        /*
        1. 	-10 + 7 * 5 //25
        2. 	(72+24) % 24 //0
        3. 	10 + -3*9 / 9  //7
        4. 	5 + 18 / 3 * 3 – (6 % 3)  //23
         */
        System.out.println(-10 + 7 * 5);
        System.out.println((72 + 24) % 24);
        System.out.println(10 + (-3 * 9) / 9);
        System.out.println(5 + 18 / 3 * 3 - (6 % 3));

        System.out.println("\n--------TASK-5--------\n");

        int num3, num4;
        System.out.println("Please enter your 1st number?");
        num3 = input.nextInt();

        System.out.println("Please enter your 2nd number?");
        num4 = input.nextInt();

        System.out.println("The average of the given numbers is: " + (num3 + num4) / 2 );

        System.out.println("\n--------TASK-6--------\n");
        /*
        6
        10
        12
        15
        17
         */
        int num5, num6, num7, num8, num9;
        System.out.println("Please enter your 1st number?");
        num5 = input.nextInt();
        System.out.println("Please enter your 2nd number?");
        num6 = input.nextInt();
        System.out.println("Please enter your 3rd number?");
        num7 = input.nextInt();
        System.out.println("Please enter your 4th number?");
        num8 = input.nextInt();
        System.out.println("Please enter your 5th number?");
        num9 = input.nextInt();

        System.out.println("The average of the given numbers is: " + (num5 + num6 + num7 + num8 +num9) / 5 );

        System.out.println("\n--------TASK-7--------\n");
        /*
        5
        6
        10
         */
        int myNumber3, myNumber4, myNumber5;
        System.out.println("Please enter your number 1 ?");
        myNumber3 = input.nextInt();
        System.out.println("The " + myNumber3 + " " + "multiplied with " + myNumber3 + " " + "is = " + (myNumber3 * myNumber3));

        System.out.println("Please enter your number 2 ?");
        myNumber4 = input.nextInt();
        System.out.println("The " + myNumber4 + " " + "multiplied with " + myNumber4 + " " + "is = " + (myNumber4 * myNumber4));

        System.out.println("Please enter your number 3 ?");
        myNumber5 = input.nextInt();
        System.out.println("The " + myNumber5 + " " + "multiplied with " + myNumber5 + " " + "is = " + (myNumber5 * myNumber5));

        System.out.println("\n--------TASK-8--------\n");

        int a;
        System.out.println("Please enter the side of square?");
        a = input.nextInt();
        System.out.println("Perimeter of the square = " + 4 * a);
        System.out.println("Area of the square = " + a * a);

        System.out.println("\n--------TASK-9--------\n");
        double averageSalary = 90000;
        System.out.println("A Software Engineer in Test can earn" + " $" + (averageSalary * 3) + " " + "in 3 years");

        System.out.println("\n--------TASK-10--------\n");
        String favBook, favColor;
        int favNum;

        System.out.println("What is your favorite book?");
        favBook = input.next();
        input.nextLine();

        System.out.println("What is your favorite color?");
        favColor = input.nextLine();

        System.out.println("What is your favorite number is?");
        favNum = input.nextInt();
        input.nextLine();

        System.out.println("User's favorite book is: " + favBook + "\nUser's favorite color is: " + favColor + "\nUser's favorite number is: " + favNum);

        System.out.println("\n--------TASK-11--------\n");

        System.out.println("Please enter your first name?");
        String fName = input.next();
        input.nextLine();
        System.out.println("Please enter your last name?");
        String lName = input.nextLine();
        System.out.println("Please enter your age?");
        int myAge = input.nextInt();
        System.out.println("Please enter your email address");
        String emailAddress = input.next();
        System.out.println("Please enter your phone number?");
        String phoneNumber = input.next();
        System.out.println("Please enter your address?");
        String myAddress = input.next();
        input.nextLine();
        /*
            User who joined this program is John Doe. John’s age is 45. John’s email
        address is johndoe@gmail.com, phone number is (123) 123 1234, and address
        is 123 St Chicago IL 12345.
         */

        System.out.println("\tUser who joined this program is " + fName + " " + lName + "." + fName + "'s age is " + myAge + ". " + fName + "'s email \naddress is " + emailAddress + ", " + "phone number is " + phoneNumber + ", and address \nis " + myAddress + "." );




    }
}
