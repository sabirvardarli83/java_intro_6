package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args){

        System.out.println("\n--------TASK-1--------\n");
        Scanner input = new Scanner(System.in);

        int num1, num2;
        System.out.println("Please enter 2 numbers");
        num1 = input.nextInt();
        num2 = input.nextInt();
        System.out.println("The difference between numbers is = " + Math.abs(num1 - num2));

        System.out.println("\n--------TASK-2--------\n");

        int number1, number2, number3, number4, number5;
        System.out.println("Please enter 5 numbers");
        number1 = input.nextInt();
        number2 = input.nextInt();
        number3 = input.nextInt();
        number4 = input.nextInt();
        number5 = input.nextInt();
        System.out.println("Max Value = " + (Math.max(Math.max(Math.max(Math.max(number1,number2),number3),number4),number5)));
        System.out.println("Min Value = " + (Math.min(Math.min(Math.min(Math.min(number1,number2),number3),number4),number5)));

        System.out.println("\n--------TASK-3--------\n");
        int random1 = ((int)(Math.random() * 51) + 50);
        int random2 = ((int)(Math.random() * 51) + 50);
        int random3 = ((int)(Math.random() * 51) + 50);

        System.out.println("Number 1 is = " + random1);
        System.out.println("Number 2 is = " + random2);
        System.out.println("Number 3 is = " + random3);
        System.out.println("The sum of number is = " + " " + (random1 + random2 + random3));

        System.out.println("\n--------TASK-4--------\n");

        double alexMoney = 125, mikeMoney = 220;
        System.out.println("Alex’s money: $" + (alexMoney -= 25.5));
        System.out.println("Mike’s money: $" +  (mikeMoney += 25.5));

        System.out.println("\n--------TASK-5--------\n");
        double price = 390;
        double dailySave = 15.60;
        System.out.println((int)(price / dailySave));

        System.out.println("\n--------TASK-6--------\n");
        String s1 = "5", s2 = "10";
        int i1 = Integer.parseInt(s1);
        int i2 = Integer.parseInt(s2);

        System.out.println("Sum of " + i1 + " " + "and " + i2 + " is = " + (i1 + i2));
        System.out.println("Product of " + i1 + " " + "and " + i2 + " is = " + (i1 * i2));
        System.out.println("Division of " + i1 + " " + "and " + i2 + " is = " + (i1 / i2));
        System.out.println("Subtraction of " + i1 + " " + "and " + i2 + " is = " + (i1 - i2));
        System.out.println("Remainder of " + i1 + " " + "and " + i2 + " is = " + (i1 % i2));

        System.out.println("\n--------TASK-7--------\n");
        String s3 = "200", s4 = "-50";
        int n1 = Integer.parseInt(s3);
        int n2 = Integer.parseInt(s4);
        System.out.println("The greatest value is = " + (Math.max(n1,n2)));
        System.out.println("The smallest value is = " + (Math.min(n1,n2)));
        System.out.println("The average is = " + ((n1 + n2) / 2));
        System.out.println("The absolute difference is = " + (Math.abs(n1 - n2)));

        System.out.println("\n--------TASK-8--------\n");
        double quarter = 0.25, dime = 0.10, nickel = 0.05, penny = 0.01;
        int aMonth = 30; // 5 months
        double dailySaving = ((3 * quarter) + (1 * dime) + (2 * nickel) + (1 * penny));
        double saveTarget1 = 24;
        double saveTarget2 = 168;

        System.out.println(((int) (saveTarget1 / dailySaving)) + " days");
        System.out.println(((int) (saveTarget2 / dailySaving)) + " days");
        System.out.println("$" + ((dailySaving * aMonth) *5));

        System.out.println("\n--------TASK-9--------\n");
        double priceComputer = 1250, savingDaily = 62.5;
        System.out.println((int) (priceComputer / savingDaily));


        System.out.println("\n--------TASK-10--------\n");
        double carPrice = 14265, paymentOption1 = 475.50, paymentOption2 = 951;

        System.out.println("Option 1 will take " + ((int) (carPrice / paymentOption1)) + " months");
        System.out.println("Option 2 will take " + ((int) (carPrice / paymentOption2)) + " months");

        System.out.println("\n--------TASK-11--------\n");

        System.out.println("Please enter 2 numbers");
        double d3 = input.nextDouble(), d4 = input.nextDouble();
        System.out.println(d3 / d4);

        System.out.println("\n--------TASK-12--------\n");

        int ran1 = ((int)(Math.random() * 101));
        int ran2 = ((int)(Math.random() * 101));
        int ran3 = ((int)(Math.random() * 101));

        if ((ran1 > 25) && (ran2 > 25) && (ran3 > 25)){
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }
        System.out.println("End of the program");

        System.out.println("\n--------TASK-13--------\n");

        System.out.println("Please enter a number between 1-7");
        int day = input.nextInt();

        if (day == 1){
            System.out.println("MONDAY");
        }
        else if (day == 2){
            System.out.println("TUESDAY");
        }
        else if (day == 3){
            System.out.println("WEDNESDAY");
        }
        else if (day == 4){
            System.out.println("THURSDAY");
        }
        else if (day == 5){
            System.out.println("FRIDAY");
        }
        else if (day == 6){
            System.out.println("SATURDAY");
        }
        else if (day == 7){
            System.out.println("SUNDAY");
        }
        else{
            System.out.println("Invalid Number!Please try again later");
        }
        System.out.println("End of the program");

        System.out.println("\n--------TASK-14--------\n");

        System.out.println("What is your exam results?");
        int result1 = input.nextInt();
        int result2 = input.nextInt();
        int result3 = input.nextInt();
        int average = (result1 + result2 + result3) / 3;
        if (average >= 70){
            System.out.println("YOU PASSED!");
        }
        else{
            System.out.println("YOU FAILED!");
        }
        System.out.println("End of the program");

        System.out.println("\n--------TASK-15--------\n");

        System.out.println("Please enter your first number");
        int a = input.nextInt();
        System.out.println("Please enter your second number");
        int b = input.nextInt();
        System.out.println("Please enter your third number");
        int c = input.nextInt();

        if (a == b && a == c){
            System.out.println("TRIPLE MATCH");
        }
        else if ((a == b) || (a == c) || (b == c)){
            System.out.println("DOUBLE MATCH");
        }
        else{
            System.out.println("NO MATCH");
        }
        System.out.println("End of the program");











    }

}
