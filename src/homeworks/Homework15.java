package homeworks;

import java.util.*;

public class Homework15 {
    public static void main(String[] args) {
        System.out.println("\n---------TASK-1------\n");
        System.out.println(Arrays.toString(fibonacciSeries1(3))); // [0, 1, 1]
        System.out.println(Arrays.toString(fibonacciSeries1(5))); // [0, 1, 1, 2, 3]
        System.out.println(Arrays.toString(fibonacciSeries1(7))); // [0, 1, 1, 2, 3, 5, 8]

        System.out.println("\n---------TASK-2------\n");
        System.out.println(fibonacciSeries2(2)); // 1
        System.out.println(fibonacciSeries2(4)); // 2
        System.out.println(fibonacciSeries2(8)); // 13

        System.out.println("\n---------TASK-3------\n");
        int[] uniqueValues = findUniques(new int[] {1, 2, 3, 4}, new int[] {3, 4, 5, 5});
        System.out.println(Arrays.toString(uniqueValues)); //

        int[] uniqueValues2 = findUniques(new int[] {8,9}, new int[] {9,8,9});
        System.out.println(Arrays.toString(uniqueValues2)); //

        System.out.println("\n---------TASK-4------\n");
        int[] number = {1, 2, 2, 3};
        System.out.println(firstDuplicate(number)); // 2

        int[] number2 = {1,  2, 3, 3, 4, 1};
        System.out.println(firstDuplicate(number2)); // 3

        System.out.println("\n---------TASK-5------\n");


    }

    public static int[] fibonacciSeries1(int n){
    if (n<0){
        return null;
    }

    int[] fibonacciNumbers = new int[n];
    fibonacciNumbers[0] = 0;
    fibonacciNumbers[1] = 1;

    for(int i = 2; i < n; i++){
        fibonacciNumbers[i] = fibonacciNumbers[i-1] + fibonacciNumbers[i-2];
    }
        return fibonacciNumbers;
    }
    public static int fibonacciSeries2(int n){
        if (n<0){
            return -1;
        }
        int num1 = 0;
        int num2 = 1;
        for (int i = 2; i < n; i++) {
            int num3 = num1 + num2;
            num1 = num2;
            num2 = num3;
        }
        return num2;
    }
    public  static int[] findUniques(int[] array1, int[] array2){
    if(array1.length == 0 && array2.length == 0){
        return new int[0];
    }
        Set<Integer> uniques = new HashSet<>();

        for (int num : array1) {
            uniques.add(num);
        }

        for (int num : array2) {
            uniques.add(num);
        }

        List<Integer> uniqueList = new ArrayList<>(uniques);
        int[] uniqueArray = new int[uniqueList.size()];

        for (int i = 0; i < uniqueList.size(); i++) {
            uniqueArray[i] = uniqueList.get(i);
        }

        return uniqueArray;

    }

    public static int firstDuplicate(int[] array){
       if (array.length < 2) {
           return -1;
       }
        Set<Integer> set = new HashSet<>();

        for (int num : array) {
            if (set.contains(num)) {
                return num;
            }
            set.add(num);
        }

        return -1;
    }

}
