package homeworks;

import utilities.ScannerHelper;

public class Homework04 {
    public static void main(String[] args) {
        System.out.println("\n--------TASK-1--------\n");

        String s1 = ScannerHelper.getFirstName();

        System.out.println("The length of the name is = " + s1.length());
        System.out.println("The first character in the name is = " + s1.charAt(0));
        System.out.println("The last character in the name is = " + s1.charAt(4));
        System.out.println("The first 3 characters in the name are = " + s1.substring(0,3));
        System.out.println("The last 3 characters in the name are = " + s1.substring(2));

        if (s1.charAt(0) == 'A' || s1.charAt(0) == 'a') System.out.println("You are in the club!");
        else System.out.println("Sorry, you are not in the club");

        System.out.println("\n--------TASK-2--------\n");
        String address = ScannerHelper.getAddress().toLowerCase();

        if (address.contains("chicago")){
            System.out.println("You are in the club");
        }
        else if (address.contains("des plaines")){
            System.out.println("you are welcome to join the club");
        }
        else{
            System.out.println("Soryy,you will never be in the club");
        }
        System.out.println("End of the program");



        System.out.println("\n--------TASK-3--------\n");

        String country = ScannerHelper.getFavoriteCountry().toLowerCase();
        if (country.contains("a") && country.contains("i")){
            System.out.println("A and i are there");
        }
        else if (country.contains("i")){
            System.out.println("I is there");
        }
        else if (country.contains("a")){
            System.out.println("A is there");
        }
        else{
            System.out.println("A and i are not there");
        }
        System.out.println("End of the program");

        System.out.println("\n--------TASK-4--------\n");

        String str = " Java is FUN ";
        String firstWord = str.trim().substring(0,4).toLowerCase();
        String secondWord = str.trim().substring(5,7).toLowerCase();
        String thirdWord = str.trim().substring(8).toLowerCase();

        System.out.println("The first word in the str is = " + firstWord);
        System.out.println("The second word in the str is = " + secondWord);
        System.out.println("The third word in the str is = " + thirdWord);
    }
}
