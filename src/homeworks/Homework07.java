package homeworks;

import primitives.Characters;

import java.util.ArrayList;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {
        System.out.println("\n------TASK-1------\n");
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(10);
        numbers.add(23);
        numbers.add(67);
        numbers.add(23);
        numbers.add(78);

        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);

        System.out.println("\n------TASK-2------\n");
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Blue");
        colors.add("Brown");
        colors.add("Red");
        colors.add("White");
        colors.add("Black");
        colors.add("Purple");
        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n------TASK-3------\n");
        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(23);
        nums.add(-34);
        nums.add(-56);
        nums.add(0);
        nums.add(89);
        nums.add(100);
        System.out.println(nums);
        Collections.sort(nums);
        System.out.println(nums);

        System.out.println("\n------TASK-4------\n");
        ArrayList<String> cities = new ArrayList<>();
        cities.add("Istanbul");
        cities.add("Berlin");
        cities.add("Madrid");
        cities.add("Paris");
        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n------TASK-5------\n");
        ArrayList<String> marvels = new ArrayList<>();
        marvels.add("Spider Man");
        marvels.add("Iron Man");
        marvels.add("Black Panter");
        marvels.add("Deadpool");
        marvels.add("Captain America");
        System.out.println(marvels);
        System.out.println(marvels.contains("Wolwerine")); // false

        System.out.println("\n------TASK-6------\n");
        ArrayList<String> avengers = new ArrayList<>();
        avengers.add("Hulk");
        avengers.add("Black Widow");
        avengers.add("Captain America");
        avengers.add("Iron Man");
        Collections.sort(avengers);
        System.out.println(avengers);
        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));

        System.out.println("\n------TASK-7------\n");
        ArrayList<Character> chars = new ArrayList<>();
        chars.add('A');
        chars.add('x');
        chars.add('$');
        chars.add('%');
        chars.add('9');
        chars.add('*');
        chars.add('+');
        chars.add('F');
        chars.add('G');
        System.out.println(chars);
        for (int i = 0; i < chars.size(); i++) {
            System.out.println(chars.get(i));
        }
        System.out.println("\n------TASK-8------\n");
        ArrayList<String> objects = new ArrayList<>();
        objects.add("Desk");
        objects.add("Laptop");
        objects.add("Mouse");
        objects.add("Monitor");
        objects.add("Mouse-pad");
        objects.add("Adapter");
        System.out.println(objects);
        Collections.sort(objects);
        int count1 = 0;
        int count2 = 0;
       for (String object : objects){
           if (object.startsWith("M") || object.startsWith("m")) count1++;
           if (!object.toLowerCase().contains("a") || !object.toLowerCase().contains("e"))
               count2++;
       }
        System.out.println(count1);
        System.out.println(count2);

        System.out.println("\n------TASK-9------\n");
        ArrayList<String> kObjects = new ArrayList<>();
        kObjects.add("Plate");
        kObjects.add("spoon");
        kObjects.add("fork");
        kObjects.add("Knife");
        kObjects.add("cup");
        kObjects.add("Mixer");
        System.out.println(kObjects);
        int countUpper = 0;
        int countLower = 0;
        int countP = 0;
        int countPp = 0;
        for (String list : kObjects){
            if (Character.isUpperCase(list.charAt(0))) countUpper++;
            if (Character.isLowerCase(list.charAt(0))) countLower++;
            if (list.toLowerCase().contains("p")) countP++;
            if (list.toLowerCase().startsWith("p") || list.toLowerCase().endsWith("p")) countPp++;
        }
        System.out.println("Elements starts with uppercase = " + countUpper);
        System.out.println("Elements starts with lowercase = " + countLower);
        System.out.println("Elements having P or p = " + countP);
        System.out.println("Elements starting or ending with P or p = " + countPp);

        System.out.println("\n------TASK-10------\n");
        ArrayList<Integer> digits = new ArrayList<>();
        digits.add(3);
        digits.add(5);
        digits.add(7);
        digits.add(10);
        digits.add(0);
        digits.add(20);
        digits.add(17);
        digits.add(10);
        digits.add(23);
        digits.add(56);
        digits.add(78);
        System.out.println(digits);
        int divide10 = 0;
        int bigger15 = 0;
        int less20 = 0;
        int b15L50 = 0;
        for (Integer element : digits){
            if (element % 10 == 0) divide10++;
            if (element % 2 == 0 && element > 15) bigger15++;
            if (element % 2 == 1 && element < 20) less20++;
            if (element < 15 || element > 50) b15L50++;
        }
        System.out.println("Elements that can be divided by 10 = " + divide10);
        System.out.println("Elements that are even and greater than 15 = " + bigger15);
        System.out.println("Elements that are odd and less than 20 = " + less20);
        System.out.println("Elements that are less than 15 or greater than 50 = " + b15L50);

    }
}
