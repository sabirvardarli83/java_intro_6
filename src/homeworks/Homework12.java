package homeworks;

import java.util.Arrays;
import java.util.TreeSet;

public class Homework12 {
    public static void main(String[] args) {
        System.out.println("\n--------TASK-1-------\n");

        System.out.println(noDigit("123Hello")); // Hello
        System.out.println(noDigit("123Hello World149")); //Hello World
        System.out.println(noDigit("Java")); // Java

        System.out.println("\n--------TASK-2-------\n");
        System.out.println(noVowel("JAVA")); // JV
        System.out.println(noVowel("125$")); // 125$
        System.out.println(noVowel("TechGlobal")); // TchGlbl

        System.out.println("\n--------TASK-3-------\n");
        System.out.println(sumOfDigits("John’s age is 29")); // 11
        System.out.println(sumOfDigits("")); // 0
        System.out.println(sumOfDigits("Java")); // 0

        System.out.println("\n--------TASK-4-------\n");
        System.out.println(hasUpperCase("java")); // false
        System.out.println(hasUpperCase("John’s age is 29")); // true
        System.out.println(hasUpperCase("$125.0")); // false

        System.out.println("\n--------TASK-5-------\n");
        int result = middleInt(-1, 25, 10);
        System.out.println(result); // 10

        System.out.println("\n--------TASK-6-------\n");
        int[] arr = {13, 13, 13 , 13, 13};// [0, 0, 0, 0, 0]
        int[] arr2 = {13, 89, 3 , 5, 13}; // [0, 89, 3, 5, 0]
        System.out.println(Arrays.toString(no13(arr)));
        System.out.println(Arrays.toString(no13(arr2)));

        System.out.println("\n--------TASK-7-------\n");

        System.out.println("\n--------TASK-8-------\n");
        String x = "abc123$#%";
        String y = "12ab$%3c%";
        System.out.println(Arrays.toString(categorizeCharacters(x))); // [nullabc, null123, null$#%]
        System.out.println(Arrays.toString(categorizeCharacters(y))); // [nullabc, null123, null$%%]


    }

    public static String noDigit(String str) {
        String empty = "";

        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c))
                empty += c;
        }
        return empty;
    }

    public static String noVowel(String str) {
        return str.replaceAll("[aeuioAEUIO]", "");
    }

    public static int sumOfDigits(String str) {
        int number = 0;
        for (char c : str.toCharArray()) {

            if (Character.isDigit(c)) {
                int nums = Character.getNumericValue(c);
                number += nums;
            }
        }

        return number;
    }

    public static boolean hasUpperCase(String str) {
        for (char c : str.toCharArray()) {
            if (Character.isUpperCase(c)) return true;
        }
        return false;
    }

    public static int middleInt(int a, int b, int c) {
        int min = Math.min(Math.min(a, b), c);
        int max = Math.max(Math.max(a, b), c);

        return a + b + c - min - max;
    }
    public static int[] no13(int[] nums){
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 13) nums[i] = 0;
        }
        return nums;
    }
    public static String[] categorizeCharacters(String str){
        String[] categorizeS = new String[3];
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i))) categorizeS[0] += str.charAt(i);
            else if (Character.isDigit(str.charAt(i))) categorizeS[1] += str.charAt(i);
            else categorizeS[2] += str.charAt(i);
        }
    return categorizeS;
    }

}

