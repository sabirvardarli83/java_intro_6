package homeworks;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;

public class Homework11 {
    public static void main(String[] args) {
        System.out.println("\n-------TASK-01------\n");
        System.out.println(noSpace("  Hello  ")); // Hello
        System.out.println(noSpace("")); //
        System.out.println(noSpace(" Hello World  ")); // HelloWorld
        System.out.println(noSpace("Tech Global")); // TechGlobal

        System.out.println("\n-------TASK-02------\n");
        System.out.println(replaceFirstLast(""));// empty
        System.out.println(replaceFirstLast("A"));// empty
        System.out.println(replaceFirstLast("  A   "));// empty
        System.out.println(replaceFirstLast("Tech Global"));//lech GlobaT

        System.out.println("\n-------TASK-03------\n");
        System.out.println(hasVovel("")); // f
        System.out.println(hasVovel("java")); // t
        System.out.println(hasVovel("1234")); // f
        System.out.println(hasVovel("ABC")); // t

        System.out.println("\n-------TASK-04------\n");
        checkAge(2010); // AGE IS NOT ALLOWED
        checkAge(2006); // AGE IS ALLOWED
        checkAge(2060); // this part is wrong
        checkAge(1920); // AGE IS NOT VALID

        System.out.println("\n-------TASK-05------\n");

        System.out.println(averageOfEdges(0, 0, 0));// 0
        System.out.println(averageOfEdges(-2, -2, 10));// 4
        System.out.println(averageOfEdges(-3,15,-3));//-3 why i dont know
        System.out.println(averageOfEdges(10, 13, 20));//15

        System.out.println("\n-------TASK-06------\n");
        String[] arr = {"appium", "123", "ABC", "java"};
        System.out.println(Arrays.toString(noA(arr))); // [###, 123, ###, java]

        System.out.println("\n-------TASK-07------\n");
        int[] arr1 = {10, 11, 12, 13, 14, 15};
        System.out.println(Arrays.toString(no3or5(arr1))); // [99, 11, 100, 13, 14, 101]

        System.out.println("\n-------TASK-08------\n");





    }







    public static String noSpace(String str){

        return str.trim().replaceAll("\\s+", "");
    }

    public static String replaceFirstLast(String str){
        str = str.trim();
        if(str.length() < 2) return "";
        else return str.charAt(str.length() - 1) + str.substring(1, str.length() - 1) + str.charAt(0);
    }

    public static boolean hasVovel(String str){
        str = str.toLowerCase();
        if (str.contains("a") || str.contains("e") || str.contains("i") || str.contains("o") || str.contains("u")) return true;
        else return false;

    }

    public static void checkAge(int yearOfBirth){
        LocalDate currentDate = LocalDate.now();
        int age = currentDate.getYear() - yearOfBirth;
        if (age < 16) System.out.println("AGE IS NOT ALLOWED");
        else if (age >= 16 && age <= 100) System.out.println("AGE IS ALLOWED");
        else System.out.println("AGE IS NOT VALID");

    }
    public static int averageOfEdges(int n1, int n2, int n3){
        return ((Math.max(Math.max(n1,n1),n3)) + (Math.min(Math.min(n1,n2),n3))) / 2;
    }

    public static String[] noA(String[] str){

        for (int i = 0; i < str.length; i++) {
            if (str[i].toLowerCase().startsWith("a")) str[i] = "###";
        }
            return str;
        }
        public static int[] no3or5(int[] nums){
            for (int i = 0; i < nums.length; i++) {
                if(nums[i] % 15 == 0) nums[i] = 101;
                else if (nums[i] % 3 == 0) nums[i] = 100;
                else if (nums[i] % 5 == 0) nums[i] = 99;
                else nums[i] = nums[i];
            }
            return nums;
        }
}
