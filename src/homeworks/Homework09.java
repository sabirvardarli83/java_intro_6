package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework09 {
    public static void main(String[] args) {
        System.out.println("\n------TASK-1------\n");
        int[] numbers1 = {-4, 0, -7, 0, 5, 10, 45, 45};
        int[] numbers2 = {-8, 56, 7, 8, 65};
        int[] numbers3 = {3, 4, 3, 3, 5, 5, 6, 6, 7};
        System.out.println(firstDuplicatedNumber(numbers1));
        System.out.println(firstDuplicatedNumber(numbers2));
        System.out.println(firstDuplicatedNumber(numbers3));

        System.out.println("\n------TASK-2------\n");
        String[] words1 = {"Z", "abc", "z", "123", "#" };
        String[] words2 = {"xyz", "java", "abc"};
        String[] words3 = {"a", "b", "B", "XYZ", "123"};
        System.out.println(firstDuplicatedString(words1));
        System.out.println(firstDuplicatedString(words2));
        System.out.println(firstDuplicatedString(words3));

        System.out.println("\n------TASK-3------\n");
        int[] nbr1 = {0, -4, -7, 0, 5, 10, 45, -7, 0};
        int[] nbr2 = {1, 2, 5, 0, 7};
        System.out.println(duplicatedNumbers(nbr1));
        System.out.println(duplicatedNumbers(nbr2));

        System.out.println("\n------TASK-4------\n");
        String[] words4 = {"A", "foo", "12" , "Foo", "bar", "a", "a", "java"};
        String[] words5 = {"python", "foo", "bar", "java", "123" };
        System.out.println(duplicatedStrings(words4));
        System.out.println(duplicatedStrings(words5));

        System.out.println("\n------TASK-5------\n");
        String[] words6 = {"abc", "foo", "bar"};
        String[] words7 = {"java", "python", "ruby"};
        reversedArray(words6);
        reversedArray(words7);

        System.out.println("\n------TASK-6------\n");
        String str1 = "Java is fun";
        String str2 = "Today is a fun day";
        System.out.println(reverseStringWords(str1));
        System.out.println(reverseStringWords(str2));






    }

    public static int firstDuplicatedNumber(int[] nums){
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length ; j++) {
                if (nums[i] == nums[j]) return nums[j];

            }

        }
        return -1;
    }

    public static String firstDuplicatedString(String[] str){
        for (int i = 0; i < str.length; i++) {
            for (int j = i+1; j < str.length; j++) {
                if (str[i].equalsIgnoreCase(str[j])) return str[i];

            }

        }
        return "There is no duplicates";
    }

    public static ArrayList<Integer> duplicatedNumbers(int[] nums){
    ArrayList<Integer> duplicates = new ArrayList<>();
        for (int i = 0; i < nums.length-1; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if (nums[i] == nums[j] && !duplicates.contains(nums[i]))
                    duplicates.add(nums[i]);

            }

        }
        return duplicates;
    }

    public static ArrayList<String> duplicatedStrings(String[] args){
        ArrayList<String> duplicates = new ArrayList<>();
        for (int i = 0; i < args.length-1; i++) {
            for (int j = i+1; j < args.length; j++) {
                if (args[i].equalsIgnoreCase(args[j]) && !duplicates.contains(args[i]))
                    duplicates.add(args[i]);

            }

        }
        return duplicates;
    }

    public static void reversedArray(String[] arr){
        Collections.reverse(Arrays.asList(arr));
        System.out.println(Arrays.toString(arr));
    }
    public static String reverseStringWords(String str){
        String[] words = str.split("\\s+");
        String reversedStr = "";

        for (String word : words) {
            reversedStr += new StringBuffer(word).reverse() + " ";

        }

        return reversedStr.trim();


    }

}
