package homeworks;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.println("\n------TASK-1-----\n");
        String consonants = "JAVA";//2
        System.out.println(countConsonants(consonants));


        System.out.println("\n------TASK-2-----\n");
        String str = "Hello, nice to meet you!!";
        String[] words = wordArray(str);
        System.out.println(Arrays.toString(words));

        System.out.println("\n------TASK-3-----\n");
        String sTrim = "Hello,    nice to   meet     you!!";
        System.out.println(removeExtraSpaces(sTrim));


        System.out.println("\n------TASK-4-----\n");
        int count = count3OrLess();
        System.out.println(count);


        System.out.println("\n------TASK-5-----\n");
        String dateOfBirth = "12/16/19500";
        boolean result = isDateFormatValid(dateOfBirth);
        System.out.println(result);

        System.out.println("\n------TASK-6-----\n");
        String email = "abc@gmail.com";
        boolean emailCheck = isEmailFormatValid(email);
        System.out.println(emailCheck);


        /*
        Write a method named isDateFormatValid() that takes a String dateOfBirth as
        an argument and checks if the given date matches the given DOB requirements.
        This method would return a true or false boolean
        Format: nn/nn/nnnn
         */

    }

    public static int countConsonants(String str){
        int count = 0;
        Pattern pattern = Pattern.compile("[a-zA-Z&&[^aeiouAEIOU]]");
        Matcher matcher = pattern.matcher(str);
        while(matcher.find()){
            count++;
        }
        return count;
    }

    public static String[] wordArray(String str){
        String[] words = str.trim().split("\\s+");
        return words;
    }

    public static String removeExtraSpaces(String str){
        String s = str.trim().replaceAll("\\s+", " ");
        return s;
    }

    public static int count3OrLess(){
        int count = 0;

     Pattern pattern = Pattern.compile("\\b\\w{1,3}\\b");
     Matcher matcher = pattern.matcher(ScannerHelper.getSentence());

     while(matcher.find()){
         System.out.println(matcher.group());
         count++;
     }
     return count;
    }

    public static boolean isDateFormatValid(String dateOfBirth){
        String reg = "[0-9]{2}?/[0-9]{2}/[0-9]{4}";
        return dateOfBirth.matches(reg);

    }
/*
Format: <2+chars>@<2+chars>.<2+chars>

 */
    public static boolean isEmailFormatValid(String email){
        String regex = "[a-zA-Z]{2,}?@[a-zA-Z]{2,}[.][a-zA-Z]{2,}";
        return email.matches(regex);
    }


}
