package string_Methods;

public class _02_Concat_Method {
    public static void main(String[] args) {

        /*
        return type
        returning String
        non-static
        it takes a String as an argument
         */
        String str1 = "Tech";
        String str2 = "Global";

        System.out.println(str1.concat(str2));//the same with below
        System.out.println(str1 + str2);

    }
}
