package string_Methods;

import utilities.ScannerHelper;

public class Exercise05 {
    public static void main(String[] args) {
         /*
        Create a program to ask the user to enter a String
        if the word starts with a and ends with e then print  out true
        if not print out false
        EX:
        Program:Please enter a String
        User: apple

        */
        String s1 = ScannerHelper.getString().toLowerCase();//lowercase helps us find lowercase
        //if (s1.startsWith("a") && s1.endsWith("e")) System.out.println("True");
        //else System.out.println("False");
        System.out.println(s1.startsWith("a") && s1.endsWith("e"));// it turns true or false no need if else


    }
}
