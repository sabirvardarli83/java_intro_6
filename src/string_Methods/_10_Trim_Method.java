package string_Methods;

public class _10_Trim_Method {
    public static void main(String[] args) {
        /*
        return type
        return a String
        non-static
        no arguments
         */



        String str = "TechGlobal    ";
        System.out.println(str.trim());//TechGlobal -> removes the spaces on the sides. if space is between the letter
        //does not work

        String str2 = "     Hel lo Wor ld   ";
        System.out.println(str2.trim());// Hel lo Wor ld
    }
}
