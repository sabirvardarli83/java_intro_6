package string_Methods;

import utilities.ScannerHelper;

public class ExerciseCharAt {
    public static void main(String[] args) {
        String str = "TechGlobal";
        String str2 = "Hello World";
        System.out.println(str.charAt(4));//G

        //print out the last character of the string

        System.out.println(str.charAt(9));//l

        // ----------Way-2----------
        System.out.println(str.charAt(str.length()-1)); // l
        System.out.println(str2.charAt(str2.length()-1)); // d

        /*
        Ask the user to enter a string and print out the last character of that string
         */
        String str3 = ScannerHelper.getString();
        System.out.println(str3.charAt(str3.length()-1));

    }
}
