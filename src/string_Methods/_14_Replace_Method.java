package string_Methods;

public class _14_Replace_Method {
    public static void main(String[] args) {

        /*
        return type
        return string
        non-static
        takes 2 strings or 2 chars argument
         */

        String str = "ABC123";

        //str = str.replace("ABC", "abc"); it depends on the case we may not use this way.

        System.out.println(str.replace("ABC", "abc"));//abc123

        System.out.println(str.replace("2", ""));//ABC13


    }
}
