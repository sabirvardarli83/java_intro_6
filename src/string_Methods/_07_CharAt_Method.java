package string_Methods;

public class _07_CharAt_Method {
    public static void main(String[] args) {

        /*
        1-return type
        2-returns char
        3-non-static
        4-it takes an int index as an argument
         */

        String name = "Sabir";
        char firstLetter = name.charAt(0); //S
        char secondLetter = name.charAt(1); //a
        char thirdLetter = name.charAt(2); //b
        char fourthLetter = name.charAt(3); //i
        char fifthLetter = name.charAt(4); //r
        System.out.println(name); //Sabir
        System.out.println(name.charAt(0)); // we can also print as firstLetter
        System.out.println(secondLetter);
        System.out.println(thirdLetter);
        System.out.println(fourthLetter);
        System.out.println(fifthLetter);

        String str = "Hello World";
        System.out.println(str.charAt(9)); //l
        //System.out.println(str.charAt(-3)); gives error
    }
}
