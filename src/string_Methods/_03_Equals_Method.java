package string_Methods;

public class _03_Equals_Method {
    public static void main(String[] args) {

        /*
        1- return type ->str1.equals(str2)
        2- return -> boolean
        3- non-static (no class)
        4- takes objects as argument but in our case it takes String
         */
        String str1 = "Tech";
        String str2 = "Global";
        String str3 = "tech";
        String str4 = "TechGlobal";
        String str5 = "Tech";


        boolean isEquals = str1.equals(str2);
        System.out.println(isEquals); // false
        System.out.println(str1.equals(str3));//false

        System.out.println(!str1.equals(str5));//false it is equal
    }
}
