package string_Methods;

import utilities.ScannerHelper;

public class _13_Contains_Method {
    public static void main(String[] args) {
        /*
        return type
        return boolean
        non-static
        takes string argument
         */
        String str = "John Doe";
        //str = str.toLowerCase() turns lowercase
        // or boolean containJohn = str.toLowerCase().contains("john);
        boolean containJohn = str.contains("John");// Case-sensitive. this is true but if we type as lowercase gives false.
        System.out.println(containJohn);


        /*
        write a program to ask the user for a string
        return true if this string is a sentence and false if it is a single word

        ask the user using ScannerHelper
        check if string contains spaces
        check if string ends with period

         */
        String s = ScannerHelper.getString();
        System.out.println(s.contains(" ") && s.endsWith("."));
    }
}
