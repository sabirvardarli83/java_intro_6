package string_Methods;

public class _06_ToUpperCase_Method {
    public static void main(String[] args) {
        /*
        1-return type
        2-return string
        3-non-static
        4-no argumemnts

         */
        // if we put the method inside println or return an object that means return type
        // if we call a class name it means it is static.
        System.out.println("HelloWorld".toUpperCase());//prints -> HELLOWORLD no need to create a String object.
        System.out.println("".toUpperCase());// empty space

        String s1 = "HELLO";
        String s2 = "hello";
        if (s1.toLowerCase().equals(s2.toLowerCase())) System.out.println("EQUAL"); // we can do it also uppercase too
        else System.out.println("NOT EQUAL");

    }
}
