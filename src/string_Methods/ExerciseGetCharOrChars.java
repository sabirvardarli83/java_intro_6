package string_Methods;

public class ExerciseGetCharOrChars {
    public static void main(String[] args) {
        String s1 = "sabir";        // length 5      | middle char is index of 2
        String s2 = "Matthew";      // length 7      | middle char is index of 3
        String s3 = "Ronaldo!!";    // length 9      | middle char is index of 4

        // to find the middle character of an odd word we would need to do (length()-1)/2
        System.out.println(s1.charAt((s1.length()-1)/2)); // b

        // to find the middle character of an even word we would first get length()/2 - 1
        // then we will get the length()/2
        String s4 = "Okan";         // length 4      | middle char is index of 2
        String s5 = "yousef";
        System.out.println("" + s5.charAt(s5.length()/2 - 1) + s5.charAt(s5.length()/2)); //us
        System.out.println("" + s4.charAt(s4.length()/2 - 1) + s4.charAt(s4.length()/2)); //ka.
        // DON'T FORGET CONCAT("" + .....) AT FIRST,OTHERWISE IT TURNS NUMBER
    }
}
