package string_Methods;

public class _09_Length_Method {
    public static void main(String[] args) {
        /*
        return type
        return int
        non static
        no arguments
         */

        String str = "TechGlobal";
        System.out.println(str.length());//10

        String str2 = "I am learning java and it is fun";
        System.out.println(str2.length());//32
        String str3 = " $%()";
        System.out.println(str3.length());//5. there is also space after "
    }


}
