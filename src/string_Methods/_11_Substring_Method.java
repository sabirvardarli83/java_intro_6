package string_Methods;

public class _11_Substring_Method {
    public static void main(String[] args) {
       /*
       return type
       return a  String
       non-static
       takes int argument
        */


        //TechGlobal can be; Tech or Global or ch or echGlo. Case-sensitive

        String str = "I love Java";
        String firstWord = str.substring(0,1);//I
        String secondWord = str.substring(2,6);//love
        String thirdWord = str.substring(7,11);//java
        System.out.println(firstWord);//I -> 0 is inclusive 1 is not.
        System.out.println(secondWord);//love
        System.out.println(thirdWord);//java

        System.out.println(str.substring(str.length()/2));// e Java


    }
}
