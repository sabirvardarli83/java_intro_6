package string_Methods;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {
        /*
        Write a Java program that asks user to enter their favorite
        book name and quote
        And store answers of user in different Strings
        Finally, print the length of those Strings with proper
        messages
         */
        String s1 = ScannerHelper.getFavoriteBook();
        String s2 = ScannerHelper.getQuote();

        System.out.println("The length of your favorite book is  " + s1.length());
        System.out.println("The length of your favorite quote is  " + s2.length());
    }
}
