package string_Methods;

public class Exercise04 {
    public static void main(String[] args) {
        /*
        assume you have the string "I go to TechGlobal"
        extract from th string into other strings and print them out on different lines
         */
        String text = "I go to TechGlobal";
        System.out.println(text.substring(0,1));//I
        System.out.println(text.substring(2,4));//go
        System.out.println(text.substring(5,7));//to
        System.out.println(text.substring(8));//TechGlobal
    }
}
