package string_Methods;

public class MethodChaining {
    public static void main(String[] args) {
        String str = "techGlobal";

        // single method
        System.out.println(str.toLowerCase()); // techglobal

        // 2 method chained
        System.out.println(str.toLowerCase().contains("tech")); // true after contains or boolean we can not add any method

        // 3 method chained
        System.out.println(str.toUpperCase().substring(4).length()); // uppercase is string + substring string
        // but length is int so we can not continue. we print GLOBAL and it is length is 6.

        // method chained
        String sentence = "Hello, my name is John Doe. I am 30 years and i go to school at TechGlobal";
        System.out.println(sentence.toLowerCase().replace("a", "X")
                .replace("e", "X").replace("i", "X").replace("o", "X")
                .replace("u", "X"));
    }
}
