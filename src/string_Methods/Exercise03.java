package string_Methods;

public class Exercise03 {
    public static void main(String[] args) {
        /*
    Assume you are given below sentence
    "The best is Java"
    Write a Java program that extracts "Java" from given sentence
    And store extracted value in another String
    Finally, print the extracted String
     */

        String str = "The best is Java";
        String word = str.substring(12);//we don't need to end index because Java is the last word
        System.out.println(word);
        // ---------2nd way--------

        word = str.substring(str.indexOf("Java"));//Java
        System.out.println(word);
        // ---------3rd way--------

        word = str.substring(str.lastIndexOf(' ') + 1);//Java
        System.out.println(word);

        word = str.substring(str.lastIndexOf(' ')).trim();//Java
        System.out.println(word);
    }
}
