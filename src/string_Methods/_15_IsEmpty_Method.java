package string_Methods;

public class _15_IsEmpty_Method {
    public static void main(String[] args) {
        /*
        return type
        return boolean
        non-static
        no arguments
         */
        String empty = "";
        String word = "Hello";

        System.out.println("First String is empty = " + empty.isEmpty()); // true
        System.out.println("Second String is empty = " + word.isEmpty()); // false
    }
}
