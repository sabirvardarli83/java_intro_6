package string_Methods;

public class _12_StartsAndEndsWith_Method {
    public static void main(String[] args) {
        /*
        return type
        return boolean
        non-static
        takes String or char as argument
         */
        String str = "TechGlobal";

        boolean startsWith = str.startsWith("T");
        boolean endsWith = str.endsWith("T");
        System.out.println(startsWith);
        System.out.println(endsWith);

        System.out.println(str.startsWith("Techgl"));
        System.out.println(str.endsWith("TechGlobal"));



    }
}
