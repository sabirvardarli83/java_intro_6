package string_Methods;

public class _08_IndexOf_Method {
    public static void main(String[] args) {
        /*
        1-return type
        2-return int
        3-non static
        4-it takes either a string or a char as an arguments
         */
        String str = "TechGlobal";
        //find h
        System.out.println("\n--------indexOf--------\n");

        System.out.println(str.indexOf("h"));// prints 3
        System.out.println(str.indexOf('y')); // -1 negative means there is no index
        System.out.println(str.indexOf("Tech"));// 0 it gives you the starting point. it does not give you multiple answer
        System.out.println(str.indexOf("Global"));//4 get first number again.main index is TechGlobal, T is = 0
        System.out.println(str.indexOf("global"));// -1 no lowercase g -> no index

        System.out.println("\n--------lastIndexOf--------\n");

        System.out.println(str.indexOf('l')); //5
        System.out.println(str.lastIndexOf('l')); // 9
    }
}
