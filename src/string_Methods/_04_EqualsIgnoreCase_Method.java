package string_Methods;

public class _04_EqualsIgnoreCase_Method {
    public static void main(String[] args) {

       //ABC
       //abc
       String str1 = "Hello";
       String str2 = "Hi";
       String str3 = "hello";// if we write it as " hello" with a space it turns false.
       String str4 = "HellOHI";

        System.out.println(str1.equalsIgnoreCase(str2));//false
        System.out.println(str1.equalsIgnoreCase(str3));//true
        System.out.println((str1 + str2).equals((str4))); //false
        System.out.println((str1 + str2).equalsIgnoreCase(str4)); //true

    }

}
