package regex;

import java.util.regex.Pattern;

public class PhoneNumber {
    public static void main(String[] args) {
        String phoneNUmberRegex = "[(]?[0-9]{3}[)]?-[0-9]{3}-[0-9]{4}";
        System.out.println(Pattern.matches(phoneNUmberRegex, "(123)-243-9768"));

        //(xxx)-xxx-xxxx
        //(xxx) -> 1st part [0-9]{3}
        //xxx -> 2nd part
        //xxxx
    }
}
