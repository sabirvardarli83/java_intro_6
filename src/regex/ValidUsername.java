package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidUsername {
    public static void main(String[] args) {
        /*
        Ask the user to enter a username. If the username matches the format of [a-zA-Z0-9]{5,10}, print out
        "Valid Username".
        If it does not match the desired format then print out "Error!
        Username must be 5 to 10 characters long and can only contain letters and numbers"

        Scenario 1:
        Program: Please enter a username
        User: JDoe123
        Program: Valid Username

        Scenario 2:
        Program: Please enter a username
        User: John Doe
        Program: Error! Username must be 5 to 10 characters long and
        can only contain letters and numbers
         */

        String userName = ScannerHelper.getString();
        // String regex = "[a-zA-Z0-9]{5,10}"; // we can create a String container to simplify the code

        System.out.println("\n====Way-1----\n");

        if (Pattern.matches("[a-zA-Z0-9]{5,10}",userName))
        System.out.println("Valid Username");
        else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");

        System.out.println("\n====Way-2----\n");

        if (userName.matches("[a-zA-Z0-9]{5,10}")) System.out.println("Valid Username");
        else System.out.println("Error! Username ......");

        System.out.println("\n====Way-3----\n");

        Pattern pattern = Pattern.compile("[a-zA-Z0-9]{5,10}");
        Matcher matcher = pattern.matcher(userName);

        if (matcher.matches()) System.out.println("Valid Username");
        else System.out.println("Error! Username ......");
    }
}
