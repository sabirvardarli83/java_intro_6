package regex;

public class RemoveAll {
    public static void main(String[] args) {
        // counting vowels(without regex)
        String str = "Aplle";
        int count = 0;
        for (char c : str.toCharArray()){
            if (Character.toLowerCase(c) == 'a' || Character.toLowerCase(c) == 'e' || Character.toLowerCase(c) == 'i' ||
                    Character.toLowerCase(c) == 'o' || Character.toLowerCase(c) == 'u') count++;
        }
        System.out.println(count);


        // counting vowels(with regex)

          str = str.replaceAll("[^aeiouAEIOU]", ""); //Apple -> Ae
        System.out.println(str.length()); //2
    }
}
