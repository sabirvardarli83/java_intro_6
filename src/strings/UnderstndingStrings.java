package strings;

public class UnderstndingStrings {
    public static void main(String[] args) {
        String s1; //declaration of s1 as a String

        s1 = "TechGlobal School"; // initializing s1 as TechGlobal School

        String s2 = "is the best"; //declaration of s1 and initialization of s2 as is the best

        System.out.println("------Concat Using +------");
        String s3 = s1 + " " + s2;//concatenation using plus sign

        System.out.println(s3);//TechGlobal school is the best
        System.out.println("------Concat Using Method------\n");
        String s4 = s1.concat(" ").concat(s2);
        System.out.println(s4);

        System.out.println("------Concat Exercise------\n");
        String wordPart1 = "le";
        String wordPart2 = "ar";
        String wordPart3 = "ning";

        String wordPart4 = wordPart1 + wordPart2 + wordPart3;

        System.out.println(wordPart4);

        System.out.println("-------Concat Method--------\n");
        String sentencePart1 = "I can";
        String sentencePart2 = "learn Java";

        String sentencePart3 = sentencePart1.concat( " ").concat(sentencePart2);
        System.out.println(sentencePart3);



    }
}
