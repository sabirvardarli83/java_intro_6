package strings;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        String is a reference type (object) that is used to store a sequence of characters
        TEXTS
         */

        String name= "John";
        String address = "Chicago IL 12345";

        System.out.println(name);
        System.out.println(address);

        /*

        Store your favorite movie name in a String variable called as favMovie
        Then print it with a message as below

        My favorite movie = {GodFather}
         */

        String favMovie = "God Father";

        System.out.println("My favorite movie = " + favMovie);





    }
}
