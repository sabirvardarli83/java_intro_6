package methods;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {

        // I can call static method with class name
        // I can call non static methods with object
/*
Write a program that asks user to enter first name and last name
then print with the below message
The full name entered is = {result}
 */
        String fName = ScannerHelper.getFirstName(); // return a first name
        String lName = ScannerHelper.getLastName();
        System.out.println("The full name entered is = " + fName + " " + lName);

    }


}
