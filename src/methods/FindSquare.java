package methods;

import utilities.MathHelper;
import utilities.ScannerHelper;

public class FindSquare {
    public static void main(String[] args) {

        System.out.println("The square of the number is = " + MathHelper.square(ScannerHelper.getNumber()));
    }
}
