package methods;

import utilities.MathHelper;
import utilities.ScannerHelper;

public class FindProduct {
    public static void main(String[] args) {

        System.out.println("The product of 2 number is = " + MathHelper.product(ScannerHelper.getNumber(), ScannerHelper.getNumber()));
    }
}
