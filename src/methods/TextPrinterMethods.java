package methods;

import utilities.MathHelper;
import utilities.Printer;

public class TextPrinterMethods {
    public static void main(String[] args) {
        Printer.printGM();

        Printer.helloName("Jazzy"); // Hello Jazzy

        // static vs non-static
        // static methods can be invoked with the class name
        // non-static methods can be invoked with an object of the class

        Printer myPrinter = new Printer();
        myPrinter.printTechGlobal();

        // return methods vs void methods
        // if the method is return type, then we can store the returned data in a variable
        // if the method is void type then we cannot store in a variable

        int i = MathHelper.sum(3, 5); // 8
        Printer.printGM(); //it does not return anything
    }
}
