package methods;

import utilities.MathHelper;
import utilities.ScannerHelper;

public class FindSum {
    public static void main(String[] args) {


        System.out.println("The sum of 2 numbers = " + MathHelper.sum(ScannerHelper.getNumber(), ScannerHelper.getNumber()));
    }
}
