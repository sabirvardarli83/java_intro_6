package methods;

import utilities.ScannerHelper;

public class CalculateAge {
    public static void main(String[] args) {
        /*
        Write a program that asks user an age and a number

        Calculate what will be their age after number years

        Please enter an age:
        45
        Please enter a number:
        10 Age will be 55 after 10 years.
         */


        int age = ScannerHelper.getAge();
        int number = ScannerHelper.getNumber();
        System.out.println("Age will be " + (age + number) + " after " + number + " years.");


    }
}
