package sabir;

public class Exercises {
    public static void main(String[] args) {
        String input1 = "";
        String count1 = countSequenceOfCharacters(input1);
        System.out.println("Count: " + count1);

        String input2 = "abc";
        String count2 = countSequenceOfCharacters(input2);
        System.out.println("Count: " + count2);
    }

    public static String countSequenceOfCharacters(String input) {
        if (input.isEmpty()) {
            return "";
        }

        String countStr = "";
        int count;
        char currentChar, nextChar;

        for (int i = 0; i < input.length(); i++) {
            count = 1;
            currentChar = input.charAt(i);

            for (int j = i + 1; j < input.length(); j++) {
                nextChar = input.charAt(j);

                if (nextChar == currentChar) {
                    count++;
                } else {
                    break;
                }
            }

            countStr += count + "" + currentChar;
            i += count - 1;
        }

        return countStr;
    }
}