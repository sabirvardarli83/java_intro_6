package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {
        String name = "John"; //declaring and initializing the variable

        int age; // declaring the variable without a value

        age = 50; //initializing the variable

        double d1 = 10; //declaring and initializing variable
        System.out.println(d1);

        d1 = 15.5;//
        System.out.println(d1);










    }
}
