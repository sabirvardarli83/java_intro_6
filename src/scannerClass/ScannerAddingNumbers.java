package scannerClass;

import java.util.Scanner;

public class ScannerAddingNumbers {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int number1, number2, number3;
        System.out.println("Please enter number 1?");
        number1 = input.nextInt();

        System.out.println("Please enter number 2?");
        number2 = input.nextInt();

        System.out.println("Please enter number 3?");
        number3 = input.nextInt();

        System.out.println("The sum of the numbers you entered is " + (number1 + number2 + number3));


    }
}
