package projects;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {

        System.out.println("\n--------TASK-1--------\n");
        /*
        -Write a program that asks user to enter their 3 numbers
        -Then multiply these numbers and print the result in the format:
        The product of the numbers entered is = {number1}*{number2}*{number3}
        3 6 10
        */
        Scanner input = new Scanner(System.in);
        int num1, num2, num3;
        System.out.println("Please enter 3 numbers");
        num1 = input.nextInt();
        num2 = input.nextInt();
        num3 = input.nextInt();
        System.out.println("The product of the numbers entered is =" + " " + num1 * num2 * num3);

        System.out.println("\n--------TASK-2--------\n");

        /*
        -Write a program that asks user to enter their first name, last name and year of birth.
        -Then calculate the age by subtracting the year of birth from current year we are in.
        -Print result in the format: {firstName} {lastName}’s age is = {currentYear} – {yearOfBirth}.
         Jennifer Worley 1993  ---- Jennifer Worley’s age is = 29.
        */
        String fName, lName;
        int birthYear, currentYear = 2023;
        System.out.println("Please enter your first name");
        fName = input.nextLine();
        System.out.println("Please enter your last name");
        lName = input.nextLine();
        System.out.println("Please enter your year of birth");
        birthYear = input.nextInt();
        int myAge = (currentYear) - (birthYear);
        System.out.println((fName) + " " + lName + "'s age is = " + myAge + ".");

        System.out.println("\n--------TASK-3--------\n");
        /*
        -Write a program that asks user to enter their full name and weight as kg.
        -Then calculate the weight as lb. NOTE: Assume 1 kg = 2.205 lbs.
        -Print result in the format: {fullName}’s weight is = {kgWeight * 2.205} lbs.
        */
        String fullName;
        int kgWeight;
        System.out.println("What is your full name? ");
        fullName = input.nextLine();
        System.out.println("What is your weight? ");
        kgWeight = input.nextInt();
        System.out.println(fullName + "'s weight is = " + (kgWeight * 2.205) + " lbs.");


        System.out.println("\n--------TASK-4--------\n");
        /*
        -Write a program that asks 3 students to enter their full name and age
        -Then print each student’s age with their full names as {fullName}’s age is {age}.
        -And, calculate the average age for these students and print result as The average age is {averageAge}.
        -And, find the eldest age and print result as The eldest age is {eldestAge}.
        -And, find the youngest age and print result as The youngest age is {youngestAge}.
        */
        String fullName2, fullName3, fullName4;
        int age2, age3, age4;
        System.out.println("What is your full name? ");
        fullName2 = input.nextLine();
        System.out.println("What is your age? ");
        age2 = input.nextInt();
        System.out.println("What is your full name? ");
        fullName3 = input.next();
        input.nextLine();
        System.out.println("What is your age? ");
        age3 = input.nextInt();
        System.out.println("What is your full name? ");
        fullName4 = input.next();
        input.nextLine();
        System.out.println("What is your age? ");
        age4 = input.nextInt();

        System.out.println(fullName2 + "'s age is " + age2 + ".");
        System.out.println(fullName3 + "'s age is " + age3 + ".");
        System.out.println(fullName4 + "'s age is " + age4 + ".");
        System.out.println("The average age is " + ((age2 + age3 + age4) / 3) + ".");
        System.out.println("The eldest age is " + (Math.max(Math.max(age2,age3),age4)) + ".");
        System.out.println("The youngest age is " + (Math.min(Math.min(age2,age3),age4)) + ".");















    }
}
