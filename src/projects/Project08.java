package projects;

import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("\n-----TASK-1----\n");
        int[] arr = {4};
        System.out.println(findClosestDistance(arr));
        System.out.println("\n-----TASK-2----\n");
        System.out.println(findSingleNumber(new int[]{5, 3, -1, 3, 5, 7, -1}));
        System.out.println("\n-----TASK-3----\n");
        System.out.println(findFirstUniqueCharacter("abc abc d"));
        System.out.println("\n-----TASK-4----\n");
        System.out.println(findMissingNumber(new int[]{4, 7, 8, 6}));


    }
    public static int findClosestDistance(int[] nums){
        if (nums.length < 2) return -1;
        int difference = Integer.MAX_VALUE;
        Arrays.sort(nums);
        for (int i = 0; i+1 < nums.length; i++) {
            int currentDifference = nums[i+1] - nums[i];
            difference = Math.min(currentDifference,difference);

        }
        return difference;

    }
    public static int findSingleNumber(int[] arr) {

        Arrays.sort(arr);
        int single = 0;
        for (int i = 0; i < arr.length; i++) {
            int count = 0;
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    count++;
                }
            }
            if (count == 1) {
                single = arr[i];
                break;
            }
        }
        return single;
    }
    public static char findFirstUniqueCharacter(String str){

        for (int i = 0; i < str.length(); i++) {
            char c = str.toLowerCase().charAt(i);
            if (str.indexOf(c) == i && str.lastIndexOf(c) == i)
                return c;

        }
        return '\0';

    }
    public static int findMissingNumber(int[] nums){
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if (nums[i+1] - nums[i] != 1) return nums[i] + 1;
        }
        return 0;
    }
}
