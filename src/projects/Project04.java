package projects;

import utilities.RandomGenerator;
import utilities.ScannerHelper;

import java.util.Random;
import java.util.Scanner;

public class Project04 {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        System.out.println("\n--------------TASK-1--------------\n");

        //if (str.length() < 8) System.out.println("This String does not have 8 characters");
        //else System.out.println(str.substring(str.length() - 4) + str.substring(4, str.length() - 4) +
        //       str.substring(0, 4));

        System.out.println(str.length() < 8 ? "This String does not have 8 characters"
                : str.substring(str.length() - 4) + str.substring(4, str.length() - 4) + str.substring(0, 4));

        System.out.println("\n--------------TASK-2--------------\n");

        String stnc = ScannerHelper.getSentence();

        if (stnc.contains(" "))
            System.out.println(stnc.substring(stnc.lastIndexOf(" ") + 1) +
                    stnc.substring(stnc.indexOf(" "), stnc.lastIndexOf(" ") + 1) +
                    stnc.substring(0, stnc.indexOf(" ")));

        else System.out.println("This sentence does not have 2 or more words to swap");

        System.out.println("\n--------------TASK-3--------------\n");

        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I have some stupid t-shirts in the past and also some idiot look shoes";

        System.out.println(str1.replace("stupid", "nice"));
        System.out.println(str2.replace("idiot", "nice"));
        System.out.println(str3.replace("idiot", "nice")
                .replace("stupid", "nice"));

        System.out.println("\n--------------TASK-4--------------\n");
        String name = ScannerHelper.getFirstName();
        if (name.length() < 2){
            System.out.println("Invalid Input");
        }
        else if (name.length() % 2 == 1){
            System.out.println(name.charAt((name.length() - 1) / 2));
        }
        else{
            System.out.println("" + name.charAt(name.length()/2 -1) + name.charAt(name.length()/2));
        }
        System.out.println("\n--------------TASK-5--------------\n");

        String country = ScannerHelper.getFavoriteCountry();

        System.out.println(country.length() < 5 ? "Invalid Input" : country.substring(2, country.length() - 2));

        System.out.println("\n--------------TASK-6--------------\n");

        String address = ScannerHelper.getAddress();

        System.out.println(address.toLowerCase().replace("a", "*").replace("e","#")
                .replace("i", "+").replace("u", "$").replace("o", "@"));




    }
}
