package projects;

public class Project01 {
    public static void main(String[] args) {
        /*
        TASK-1
        -Store your name in a String variable called name
        -Print the variable with a proper message
        -EX/ My name is = name
        NOTE: Uppercases, lowercases and spaces are important
         */
        System.out.println("\n--------TASK-1--------\n");
        String name = "Sabir";
        System.out.println("My name is = " + name);

        /*
        TASK-2
        -Create different char variables for each of your name letter and store them in separate variables as nameCharacter1, nameCharacter2 and so on.
        -Print variables with proper messages
        -EX/Name letter 1 is = nameCharacter1
         Name letter 2 is = nameCharacter2
         */

        System.out.println("\n--------TASK-2--------\n");

        char nameChar1 = 'S';
        char nameChar2 = 'a';
        char nameChar3 = 'b';
        char nameChar4 = 'i';
        char nameChar5 = 'r';
        System.out.println("My name is " + nameChar1 + nameChar2 + nameChar3 + nameChar4 + nameChar5);

        /*
        TASK-3
        -Create different String variables to store info like myFavMovie, myFavSong, myFavCity, myFavActivity, myFavSnack.
        -Print variables with proper messages
        -EX/My favorite movie is = myFavMovie
         */
        System.out.println("\n--------TASK-3--------\n");

        String myFavMovie = "The Matrix.";
        String myFavSong = "My Heart Will Go On.";
        String myFavCity = "Istanbul.";
        String myFavActivity = "Playing with my daughter.";
        String myFavSnack = "Butter pecan cookies.";

        System.out.println("My favorite movie is" + myFavMovie);
        System.out.println("My all time favorite song is " + myFavSong);
        System.out.println("My favorite city is " + myFavCity);
        System.out.println("I love " + myFavActivity);
        System.out.println("My favorite snack is " + myFavSnack);

        /*
        TASK-4
        -Create different int variables to store info like myFavNumber, numberOfStatesIVisited, numberOfCountriesIVisited.
        -Print variables with proper messages
        -EX/My favorite number is = myFavNumber
         */

        System.out.println("\n--------TASK-4--------\n");
        int myFavNumber = 0, numberOfStatesIVisited = 8, numberOfCountriesIVisited = 6;

        System.out.println("My favorite number is " + myFavNumber);
        System.out.println("I have visited " + numberOfStatesIVisited + " States in US.");
        System.out.println("I have visited " + numberOfCountriesIVisited + " Countries.");

        /*
        TASK-5
        -Create a boolean called amIAtSchoolToday
        -Assign true to this variable if you are at campus today
        -Assign false to this variable if you are joining online today
        -Print variable value with a proper message using println() method
        EX/ I am at school today = amIAtSchoolToday
         */

        System.out.println("\n--------TASK-5--------\n");

        boolean amIAtSchoolToday = false;
        System.out.println("I am at school today = " + amIAtSchoolToday);

        /*
        TASK-6
        -Create a boolean called isWeatherNiceToday
        -Assign true to this variable if it is above 60F today
        -Assign false to this variable if it is below or equal to 60F today
        -Print variable value with a proper message using println() method
        -EX/ Weather is nice today = isWeatherNiceToday
         */


        System.out.println("\n--------TASK-6--------\n");
        boolean isWeatherNiceToday =false;
        System.out.println("Weather is nice today = " + isWeatherNiceToday);










    }

}
