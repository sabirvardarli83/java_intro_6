package projects;

import utilities.ScannerHelper;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n----------TASK-1----------\n");

        String word = ScannerHelper.getSentence();
        int count = 1;
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == ' ') count++;
        }
        word = word.trim();
        if (word.contains(" ")) System.out.println("This sentence has " + count + " words.");
        else System.out.println("This sentence does not have multiple words");

        System.out.println("\n----------TASK-2----------\n");

        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();
        String s = "";
        for (int i = Math.min(num1,num2); i <= Math.max(num1,num2); i++) {
            if (i % 5 != 0) s += i + " - ";
        }
        System.out.println(s.substring(0,s.length() - 3));

        System.out.println("\n----------TASK-3----------\n");

        int cnt = 0;
        for (int j = 0; j < word.length(); j++) {
            if (word.toLowerCase().charAt(j) == 'a') cnt++;
        }
        word = word.trim();
        if (word.length() >= 1) System.out.println("This sentence has " + cnt + " a or A letters.");
        else System.out.println("This sentence does not have any characters.");

        System.out.println("\n----------TASK-4----------\n");

        String reverse = "";

        for (int k = word.length() -1; k >= 0; --k) {
            reverse += word.charAt(k);
        }
        if(word.equals(reverse)) System.out.println("This word is palindrome");
        else System.out.println("This word is not palindrome");

        System.out.println("\n----------TASK-5----------\n");


        for (int i = 1; i < 10; i++) {
            for (int j = i; j < 10; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");

            }
            System.out.println(" ");

        }


    }
}
