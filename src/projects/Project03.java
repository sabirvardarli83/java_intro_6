package projects;

import java.util.Random;

public class Project03 {
    public static void main(String[] args) {

        System.out.println("\n--------------TASK-1--------------\n");

        String s1 = "24", s2 = "5";
        int num1 = Integer.parseInt(s1);
        int num2 = Integer.parseInt(s2);


        System.out.println("The sum of " + num1 + " and " + num2 + " = " + (num1 + num2));
        System.out.println("The subtraction of " + num1 + " and " + num2 + " = " + (num1 - num2));
        System.out.println("The division of " + num1 + " and " + num2 + " = " + ((double)num1 / num2));
        System.out.println("The multiplication of " + num1 + " and " + num2 + " = " + (num1 * num2));
        System.out.println("The remainder of " + num1 + " and " + num2 + " = " + (num1 % num2));

        System.out.println("\n--------------TASK-2--------------\n");

        Random r = new Random();

        int number = r.nextInt(35) + 1;
        System.out.println(number);

        if (number  == 2 || number == 3 || number == 5 || number == 7 || number == 11 || number == 13 || number == 17 || number == 19 || number == 23 || number == 29 || number == 31)
            System.out.println(number + " IS A PRIME NUMBER");
        else System.out.println(number + " IS NOT A PRIME NUMBER ");

        System.out.println("End of the program");

        System.out.println("\n--------------TASK-3--------------\n");

        int n1 = r.nextInt(50) + 1;
        int n2 = r.nextInt(50) + 1;
        int n3 = r.nextInt(50) + 1;
        System.out.println(n1);
        System.out.println(n2);
        System.out.println(n3);

        System.out.println("Lowest number is = " + Math.min(Math.min(n1,n2),n3));
        System.out.println("Greatest number is = " + Math.max(Math.max(n1,n2),n3));
        System.out.println("Middle number is = " + (n1 + n2 + n3 - (Math.min(Math.min(n1,n2),n3)) - (Math.max(Math.max(n1,n2),n3))));
        System.out.println("End of the program");

        System.out.println("\n--------------TASK-4--------------\n");

     char c = 'R';
     if (c >= 65 && c <= 90) System.out.println("This letter is uppercase");
     else if (c >= 97 && c<= 122) System.out.println("This letter is lowercase");
     else System.out.println("Invalid character detected!!!");
        System.out.println("End of the program");

        System.out.println("\n--------------TASK-5--------------\n");

        char c1 = 'R';
        if (c1 == 65 || c1 ==69 || c1 == 73 || c1 == 79 || c1 == 85 || c1 == 97 || c1 == 101 || c1 == 105 || c1 == 111 || c1 == 117)
            System.out.println("The letter is vowel");
        else if ((c1 >= 65 && c1 <= 90) || (c1 >= 97 && c1 <= 122))
            System.out.println("The letter is consonant");
        else System.out.println("Invalid character detected!!!");
        System.out.println("End of the program");

        System.out.println("\n--------------TASK-6--------------\n");

        char x = '*';
        if (x >= 65 && x <= 90 || x >= 97 && x <= 122 || x >= 48 && x <= 57)
            System.out.println("Invalid character detected!!!");
        else System.out.println("Special character is = " + x);
        System.out.println("End of the program");

        System.out.println("\n--------------TASK-7--------------\n");

        char y = 'g';
        if (y >= 65 && y <= 90 || y >= 97 && y <= 122 ) System.out.println("Character is a letter");
        else if (y >= 48 && y <= 57) System.out.println("Character is a digit");
        else System.out.println("Character is a special character");
        System.out.println("End of the program");
    }
}
