package projects;

import java.util.Arrays;

public class Project06 {
    public static void main(String[] args) {

        System.out.println("\n-------TASK-1-------\n");
        int[] num = {10, 7, 7, 10, -3, 10, -3};//Testing
        findGreatestAndSmallestWithSort(num);
        System.out.println("\n-------TASK-2-------\n");
        findGreatestAndSmallest(num);
        System.out.println("\n-------TASK-3-------\n");
        int[] num1 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallestWithSort(num1);
        System.out.println("\n-------TASK-4-------\n");
        findSecondGreatestAndSmallest(num1);
        System.out.println("\n-------TASK-5-------\n");
        String[] list ={"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        findDuplicatedElementsInAnArray(list);
        System.out.println("\n-------TASK-6-------\n");




    }
    public static void findGreatestAndSmallestWithSort(int[] number){
        Arrays.sort(number);
        int smallest = number[0];
        int greatest = number[number.length - 1];
        System.out.println("Smallest = " + smallest);
        System.out.println("Greatest = " + greatest);
    }
    public static void findGreatestAndSmallest(int[] number2) {
        int min = number2[0];
        int max = number2[0];
        for (int i = 0; i < number2.length; i++) {
            if (number2[i] < min) min = number2[i];
            else if (number2[i] > max) max = number2[i];
        }
        System.out.println("Smallest = " + min);
        System.out.println("Greatest = " + max);
    }
    public static void findSecondGreatestAndSmallestWithSort(int[] number3){
        Arrays.sort(number3);
        int min = number3[0];
        int max = number3[number3.length - 1];
        int sSmallest = Integer.MAX_VALUE;
        int sGreatest = Integer.MIN_VALUE;

        for (int j : number3){
            if (j != max && j > sGreatest) sGreatest = j;
            if (j != min && j < sSmallest) sSmallest = j;
        }
        System.out.println("Second Smallest = " + sSmallest);
        System.out.println("Second Greatest = " + sGreatest);
    }
    public static void findSecondGreatestAndSmallest(int[] number4){
        int min = number4[0];
        int max = number4[number4.length - 1];
        int sSmallest = Integer.MAX_VALUE;
        int sGreatest = Integer.MIN_VALUE;

        for (int j : number4){
            if (j != max && j > sGreatest) sGreatest = j;
            if (j != min && j < sSmallest) sSmallest = j;
        }
        System.out.println("Second Smallest = " + sSmallest);
        System.out.println("Second Greatest = " + sGreatest);

    }
    public static void findDuplicatedElementsInAnArray(String[] str){
    String duplicates = "";
        for (int i = 0; i < str.length; i++) {
            for (int j = i + 1; j < str.length; j++) {
                if (!duplicates.contains(str[i]) && str[i].equals(str[j])){
                  duplicates += str[i] + " ";
                    System.out.println(str[i]);
                    break;
                }

            }

        }
    }
    public static void findMostRepeatedElementInAnArray(String str2){

    }



}
