package projects;

import primitives.Characters;

import java.util.ArrayList;
import java.util.Arrays;

public class Project07 {
    public static void main(String[] args) {
        System.out.println("\n--------TASK-1------\n");
        System.out.println(countMultipleWords(new String[]{"foo", "", " ", "foo bar", "java is fun", " ruby "}));
        System.out.println("\n--------TASK-2------\n");
        System.out.println(removeNegatives(new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15))));
        System.out.println("\n--------TASK-3------\n");
        System.out.println(validatePassword("Abcd123!"));
        System.out.println("\n--------TASK-4------\n");
        System.out.println(validateEmailAddress("abc@gmail.c"));


    }
    public static int countMultipleWords(String[] array){
        int count = 0;
        for (String element : array){
            if (element.trim().contains(" ")) count++;

        }
        return count;

    }
    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> numbers){
        numbers.removeIf(x -> x < 0);
        return numbers;
    }
    public static boolean validatePassword(String str){
        int digit = 0;
        int upper = 0;
        int lower = 0;
        int spec = 0;
        if (!(str.contains(" ")) && str.length() >= 8 || str.length() <= 16){
            for (int i = 0; i < str.length(); i++) {
                if (Character.isDigit(str.charAt(i))) digit++;
                else if (Character.isUpperCase(str.charAt(i))) upper++;
                else if (Character.isLowerCase(str.charAt(i))) lower++;
                else if (!Character.isLetterOrDigit(str.charAt(i))) spec++;

            }
            return digit >= 1 && upper >= 1 && lower >= 1 && spec >= 1;
        }
        return true;

    }
    public static boolean validateEmailAddress(String email){
        int countAt = 0;
        int countL = 0;
        if (!email.contains(" ") && email.substring(0,email.indexOf("@")).length() >= 2 &&
        email.substring(email.indexOf("@"), email.indexOf(".")).length() >= 2 &&
                email.substring(email.indexOf(".")).length() >= 2){
            for (int i = 0; i < email.length(); i++) {
                if (email.charAt(i) == '@') countAt++;
                else if (email.charAt(i) == '.') countL++;

            }
            return countL == 1 && countAt == 1;
        }
        return true;

    }


}
