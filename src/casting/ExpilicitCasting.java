package casting;

public class ExpilicitCasting {
    public static void main(String[] args) {
        /*
        Explicit casting is storing bigger data type into smaller data type
        It does not happen automatically and programmer have to resolve the compiler issue
        Also known as narrowing or down-casting

        long => byte
        int => short
        double => float
        double => int
         */
        long number1 = 273645273;
        byte number2 = (byte) number1;//we used casting by using byte in parentheses

        System.out.println(number2);//-39 didn't work- byte moves between -128 to  127.We lose data

    }
}
