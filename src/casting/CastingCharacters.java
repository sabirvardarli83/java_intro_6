package casting;

public class CastingCharacters {
    public static void main(String[] args) {
        int i1 = 65;
        char c1 = (char)i1; //explicit casting
        System.out.println(c1); //A  "we use ASCII table"

        char c2 = 97; //a
        System.out.println(c2);

        char char1 = 'J'; //74
        char char2 = 'o'; //111
        char char3 = 'h'; //104
        char char4 = 'n'; //110

        System.out.println(char1 + char2 + char3 + char4);  // 399

        System.out.println("" + char1 + char2 + char3 + char4); //John - it became a String after adding "" +
        /*
        General rule about Concatenation;
        String + data => String
        String + data + data + data => String
         */


    }
}
