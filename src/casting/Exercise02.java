package casting;

public class Exercise02 {
    public static void main(String[] args) {

        /* phone price is => $899.99
        $50 per day

        how many days later you can buy this phone
        18 days we need
         */

        double price = 900;
        double dailySaveAmount = 50;

        System.out.println("You can buy the phone after " + (int) (price / dailySaveAmount)  + " days.");

    }
}
