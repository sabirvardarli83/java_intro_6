package gitDemo;

public class Demo {
    public static void main(String[] args) {
        String firstName = "Sabir ";
        String lastName = "Vardarli";
        System.out.println(firstName + lastName);
        String favColor = "black";
        System.out.println("My favorite color is " + favColor);

        /*
        I want you to print out My favorite car is " " and the model year is " "

        String favoriteCar = "Porsche";
        int year = 2023;
         */
        String favCar = "Tesla";
        int year = 2023;
        System.out.println("My fav car is " + favCar + " and the model year is " + year);


    }
}
