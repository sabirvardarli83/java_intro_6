package loops;

import utilities.ScannerHelper;

public class Exercise08_CountCharacterInAString {
    public static void main(String[] args) {
        /*
        ask user to enter a String
        count how many A or a letter you have in the given String
        Apple -> 1
        Banana -> 3
        John -> 0

        PSEUDO CODE
        check each character in the String one by one
        Increase the count of letter by one when the character is "A" or "a"
        start point:
        end point:
        update:
         */
        String str = ScannerHelper.getString();

        int count = 0;
        for (int i = 0; i <= str.length() - 1 ; i++) {
            if (str.toLowerCase().charAt(i) == 'a') count++;

        }
        System.out.println(count);
    }
}
