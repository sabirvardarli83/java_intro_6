package loops;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter 2 different positive numbers
        Print all the even numbers bt given numbers by user in ascending order

        3, 10 -> Test
        4
        6
        8
        10

        7, 2  -> Test
        2
        4
        6
        */
       int n1 = ScannerHelper.getNumber();
       int n2 = ScannerHelper.getNumber();
        for (int i = Math.min(n1, n2); i <= Math.max(n1, n2); i++) {
            if (i % 2 == 0) System.out.println(i);

        }
    }
}
