package loops.control_statements;

public class Exercise02_Continue {
    public static void main(String[] args) {
        /*
        write a program that prints all the number from 1 to 100
        but do not print any number divided by 13.
         */

        for (int i = 1; i < 101 ; i++) {
           if(i % 13 == 0) continue; //if(i % 13 != 0) sout(i) bu da gecerli
           else System.out.println(i);

        }
    }
}
