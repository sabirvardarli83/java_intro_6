package loops;

import utilities.ScannerHelper;

public class Exercise10_SumOfNumberByUser {
    public static void main(String[] args) {
        /*
        Write a program that ask a user to enter 5 numbers separately
        find sum of the given numbers by user
        2, 3, 4, 5, 6
        Output:
        20
        11, 15, 23, -7, 8
        Output:
        50
         */
        int start = 1;
        int sumWhile = 0;
        while(start <= 5){
            sumWhile += ScannerHelper.getNumber();
            start++;
        }
        System.out.println(sumWhile);

        System.out.println("\n-----for loops-----\n");
        int sum = 0;

        for (int i = 1; i <= 5; i++) { // 1, 2, 3, 4, 5
            sum += ScannerHelper.getNumber();
        }

        System.out.println(sum);
    }

}
