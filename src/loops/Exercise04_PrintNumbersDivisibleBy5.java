package loops;

public class Exercise04_PrintNumbersDivisibleBy5 {
    public static void main(String[] args) {
        /*
        Write a program check numbers from 1 to 50(50 included) and print aa the numbers
        that can be divided by 5
         */
        for (int i = 1; i < 51 ; i++) {
            if (i % 5 == 0) System.out.println(i);

        }
    }
}
