package loops;

public class Exercise03_PrintEvenNumbers {
    public static void main(String[] args) {
/*
write a program 0-10 both include and print even numbers
 */
        // 1sy way
        for (int i = 0; i < 11; i++) {
            if (i % 2 == 0) System.out.println(i);

        }
        //2nd way- but not recommended
        for (int i = 0; i < 11; i+=2) {
            System.out.println(i);

        }
    }
}
