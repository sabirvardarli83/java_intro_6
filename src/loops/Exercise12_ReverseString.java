package loops;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise12_ReverseString {
    public static void main(String[] args) {
        /*
        Write a program that reads a name from user
        Reverse the name and print it back
        Test data:
        James
        Expected Output:
        semaJ
        Test data:
        John
        Expected Output:
        nhoJ
          */
        Scanner input = new Scanner(System.in);
        String str = input.nextLine();
        String reversedString = "";
        for (int i = str.length() -1; i >= 0 ; i--) {
            reversedString += str.charAt(i);

        }
        System.out.println(reversedString);


    }
}
