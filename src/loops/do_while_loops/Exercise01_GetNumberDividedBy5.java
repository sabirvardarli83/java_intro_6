package loops.do_while_loops;

import utilities.ScannerHelper;

public class Exercise01_GetNumberDividedBy5 {
    public static void main(String[] args) {
        /*
        write a program asks user to enter a number
        keep asking till they enter a number divided by 5.
        Please enter a number:
         3

         Please enter a number:
         4

         Please enter a number:
         25

         End of the program.

         Please enter a number:
         10

         End of the program.
         */
        int num;
        do{
           num = ScannerHelper.getNumber();
        }
        while (num % 5 != 0);
        System.out.println("End of the program");


        System.out.println("\n------while loop-----\n");

        while(true){
            int n1 = ScannerHelper.getNumber();
            if(n1 % 5 == 0) {
                System.out.println("End of the program");
                break; // otherwise it doesn't stop

            }
        }
        System.out.println("\n------for loop-----\n");
        for(; ;){
            int n2 = ScannerHelper.getNumber();
            if(n2 % 5 == 0) {
                System.out.println("End of the program");
                break;
            }
        }
    }
}
