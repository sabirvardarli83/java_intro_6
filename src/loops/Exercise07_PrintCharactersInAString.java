package loops;

import utilities.ScannerHelper;

public class Exercise07_PrintCharactersInAString {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String
        print each character of the String in a separate line
        "Hello"
        H   -> s.charAt(0)
        e   -> s.charAt(1)
        l   -> s.charAt(2)
        l   -> s.charAt(3)
        o   -> s.charAt(4)

        start point: 0
        end point : s.length() - 1
        update: increment
         */
        String s = ScannerHelper.getString();
        for (int i = 0; i <= s.length() - 1 ; i++) {
            System.out.println(s.charAt(i));


        }

    }
}
