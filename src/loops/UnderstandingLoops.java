package loops;

public class UnderstandingLoops {
    public static void main(String[] args) {
        //Print Hello World 5 times

        for (int i = 0; i < 5; i++){
            System.out.println("Hello World");
        }
    }
}
