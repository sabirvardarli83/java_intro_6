package loops.while_loops;

public class Exercise01_PrintNumbersDividedBy3 {
    public static void main(String[] args) {
        /*
        write a program that prints all the numbers divided by 3,starting from 1 to 100(both include)
         */

        int n = 1;
        while(n <= 100){
          if (n % 3 == 0)  System.out.println(n);
          n++;
        }
    }
}
