package loops.practices;

public class Exercise05 {
    public static void main(String[] args) {
        /*
        Assume that you are given a number, and you are
        asked to find series of Fibonacci numbers
        •What is Fibonacci numbers: a series of numbers in
        which each number ( Fibonacci number ) is the sum
        of the two preceding numbers
        •It always starts with 0 and 1
        •EX: 0, 1, 1, 2, 3, 5, 8, 13, 21
        NOTE: Write a program that handles any n series of
        numbers
        Test data 1:
        5
        Expected output 1:
        0 – 1 – 1 – 2 – 3
        Test data 2:
        7
        Expected output 2:
        0 – 1 – 1 – 2 – 3 – 5 - 8
         */
        
        int num1 = 0;
        int num2 = 1;
        int test = 5;
        String str = "";
        for (int i = 0; i <= test; i++) {
            str += num1 + " - ";
            int sum = num1 + num2;
            num1 = num2;
            num2 = sum;
        }

        System.out.println(str.substring(0,str.length() - 3));
    }
}
