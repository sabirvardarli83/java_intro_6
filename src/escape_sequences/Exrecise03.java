package escape_sequences;

public class Exrecise03 {
    public static void main(String[] args) {
        /*
        \n
        \t
        \"

        Monday\Tuesday\Wednesday

         */
        System.out.println("\n-------TASK-1-------\n");
        System.out.println("Monday\\Tuesday\\Wednesday");


        // Good \\\ morning
        System.out.println("\n---------TASK-2--------\n");
        System.out.println("Good \\\\\\ morning");



    }
}
