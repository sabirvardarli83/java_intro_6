package escape_sequences;

import jdk.nashorn.internal.parser.JSONParser;
import sun.awt.geom.AreaOp;

public class Exercise02 {
    public static void main(String[] args) {

        //I like "Sunday" and apple/

        System.out.println("\n-----------TASK-1-------------\n"); // Separator for my tasks
        System.out.println("Steve Ballmer replaced Gates as CEO in 2000, and later envisioned a \"devices and services\" strategy.");


        System.out.println("\n---------TASK-2-----------\n");
        System.out.println("I like \"Sunday\" and apple");

        System.out.println("\n---------TASK-3-----------\n");
        System.out.println("My fav fruits are \"Kiwi\" and \"Orange\"");




    }
}
