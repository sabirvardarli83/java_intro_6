package concatenation;

public class Exercise01 {
    public static void main(String[] args) {
        System.out.println("Hello World");
        System.out.println("Hello" + "World");
        System.out.println("Hello " + "World" );
        System.out.println("Hello" + " World" );
        System.out.println("Hello" + " " + "World" );
        System.out.println("Today" + " " + "is" + " " + "Sunday");
        System.out.println("123" + " " + "12"); //123 12
        System.out.println("\t" + "12" + "\t\nab");

        /*
         System.out.println(char1 + char2 + char3 + char4);  // 399
         System.out.println("" + char1 + char2 + char3 + char4); //John - it became a String after adding "" +

         General rule about Concatenation;
         String + data => String
         String + data + data + data => String
         */



    }
}
