package random_class;

import java.util.Random;

public class RandomNumbers {
    public static void main(String[] args) {
    /*
    Write a program that generates 2 random number between 10 and 11
    If both are 10 -> print true
    Otherwise -> print false
    0-create a Random class object
    1-find how many numbers do u have in your range
    2-put that in your nextInt() method
    3-add your smallest range number

    nextInt(bound) method returns a number between 0 and bound (but bound is not included)
     */
        Random r = new Random();

        int num1 = r.nextInt(2) + 10; // numbers between 0 to 4 both inclusive
        int num2 = r.nextInt(2) + 10;

        System.out.println(num1);
        System.out.println(num2);
    }
}
