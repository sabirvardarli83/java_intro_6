package practices;

import utilities.ScannerHelper;

public class Exercise02_StringMethods {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

       if (str.length() >= 3 && str.length() % 2 == 1){
            System.out.println(str.charAt((str.length()-1)/2));

        }
        else if (str.length() >= 3 && str.length() % 2 == 0){
            System.out.println("" + str.charAt(str.length()/2 - 1) + str.charAt(str.length()/2));
            // or System.out.println(s1.substring(s1.length() / 2 - 1, s1.length() / 2 + 1);
        }
        else{
           System.out.println("Length is less than 3");
       }
        //Akin"s Way;

        if(str.length() < 3) System.out.println("Length is less than 3");
        else if(str.length() % 2 == 0) System.out.println(str.substring(str.length() / 2 - 1, str.length() / 2 + 1));
        else System.out.println(str.charAt(str.length() / 2));

    }
}
