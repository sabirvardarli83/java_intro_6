package practices;

import utilities.ScannerHelper;

import java.sql.SQLOutput;

public class Exercise01_StringMethods {
    public static void main(String[] args) {
        System.out.println("\n----------TASK-1----------\n");
        /*
        Write a program that asks user to enter a String
        Then print the String with given message below
        The string given is = {RESULT}
         */
        String str = ScannerHelper.getString();
        System.out.println("The string given is = " + str);

        System.out.println("\n----------TASK-2----------\n");
        // 1st Way
        if (str.isEmpty()) System.out.println("The string given is empty");//str.equals("") or str.length() == 0 or str.length() < 1
        else System.out.println("The length is = " + str.length());

        System.out.println(str.isEmpty() ? "The string given is empty" : "the length is = " + str.length());

        System.out.println("\n----------TASK-3----------\n");
        /*
        TASK-3
        if it is not empty, print the first character
        if it is empty, "There is no character in this String"

        */
        if (str.isEmpty()) System.out.println("there is no character in this String");
        else System.out.println("The first character = " + str.charAt(0));

        System.out.println(str.isEmpty() ? "There is no character" : "The first character = " + str.charAt(0));

        System.out.println("\n----------TASK-4----------\n");
        /*
        if it is not empty, print the last character with message -> The last character = {RESULT}
        if it is empty, "There is no character in this String"
         */

        System.out.println(!str.isEmpty() ? "The last character = " + str.charAt(str.length()-1) : "There is no character in this String");

        System.out.println("\n----------TASK-5----------\n");
    /*
    -Check if the String contains any vowel letters
    -if it has any vowel, then print "This String has vowel"
    -Else, print "This String does not have vowel"
    Vowels = a, e, i, u, o, A, E, I, U, O

    Hello -> This String has vowel
    bcd   -> This String does not have vowel
    "  "  -> This String does not have vowel
    */
        str = str.toLowerCase();
        if (str.contains("a") || str.contains("e") || str.contains("i") || str.contains("u") || str.contains("o")){
            System.out.println("This String has vowel");
        }
        else{
            System.out.println("This String does not have vowel");
        }

    }
}
