package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class _11_Remove_Elements_Iterator {
    public static void main(String[] args) {
        // Remove all the elements that are more than 10 -> [10, 5, 3, 0]
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 15, 12, 5, 3, 0));
        numbers.removeIf(x -> x > 10);
        System.out.println(numbers);

        // 2nd way
        ArrayList<Integer> newList = new ArrayList<>();

        for (Integer element : numbers) {
            if (element <= 10) newList.add(element);
        }
        System.out.println(newList);

        // 3rd way Iterator-intellij says there is a simple way
        Iterator<Integer> iterator = numbers.iterator();
        while(iterator.hasNext()){
            if(iterator.next() > 10) iterator.remove();
        }
        System.out.println(numbers);
    }
}
