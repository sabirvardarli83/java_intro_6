package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _02_Additional_Methods {
    public static void main(String[] args) {
        /*
        Create an ArrayList to store below numbers
        10
        15
        20
        10
        20
        30
        Print the ArrayList
        Print size
        expected result
        [10,15,20,10,20,30]
        6
         */
        System.out.println("\n------TASK-1-----\n");
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);
        System.out.println(numbers);
        System.out.println(numbers.size());

        System.out.println("\n------contains() method-----\n");
        /*
        check if the list contains 5
        check if the list contains 10
        check if the list contains 20
         */

        System.out.println(numbers.contains(5));// false
        System.out.println(numbers.contains(10));//true
        System.out.println(numbers.contains(20));//true

        System.out.println("\n------indexOf() & lastIndexOf() methods-----\n");
        /*
        what is the first occurrence index of the element 10
        what is the first occurrence index of the element 20
        what is the last occurrence index of the element 10
        what is the last occurrence index of the element 20

        index of 15 -> 1
        last index of 15 -> 1
        exp output
        0
        2
        3
        4
         */
        System.out.println(numbers.indexOf(10)); // 0
        System.out.println(numbers.indexOf(20)); // 2
        System.out.println(numbers.lastIndexOf(10)); // 3
        System.out.println(numbers.lastIndexOf(20)); // 4

        System.out.println(numbers.indexOf(15));//1
        System.out.println(numbers.lastIndexOf(15));//1

        System.out.println(numbers.indexOf(21));// -1
        System.out.println(numbers.lastIndexOf(21));// -1

        System.out.println("\n------TASK-2- REMOVE-----\n");
        /*
        remove 15 and 30 from the list and print the list
        exp out
        [10,20,10,20]
         */

        numbers.remove((Integer)15);//it removes 15 and returns true
        numbers.remove((Integer) 30);//it removes 30 and returns true
        numbers.remove((Integer) 100);//it does not remove it and returns false
        System.out.println(numbers);// [10, 20, 10, 20]
        numbers.remove((Integer) 10);
        System.out.println(numbers);// [20, 10, 20] removes the first one

        // Removing the element that is 20- remove all elements that are 20

        numbers.removeIf(element -> element == 20); // Lambda expression
        System.out.println(numbers); // [10]
        /*
        remove all elements from the list
        EXP
        []
         */
        System.out.println("\n----clear() & removeAll() methods-----\n");
        numbers.add(11);
        numbers.add(12);
        numbers.add(13);
        System.out.println(numbers);// [10,11,12,13]

        numbers.clear();
        //numbers.removeAll(numbers);
        System.out.println(numbers); // []





    }
}
